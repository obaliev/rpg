package org.balev.rpg.engine.util;

import org.balev.rpg.engine.domain.action.Action;
import org.balev.rpg.engine.domain.action.ChangeLocationAction;
import org.balev.rpg.engine.domain.action.FightAction;

/**
 * Created by Aleksandr Balev on 22.07.2015.
 */
public final class ActionUtils {

    private ActionUtils() {
    }

    public static boolean isChangeLocationAction(Action action) {
        return action.getClass().equals(ChangeLocationAction.class);
    }

    public static boolean isFightAction(Action action) {
        return action.getClass().equals(FightAction.class);
    }
}
