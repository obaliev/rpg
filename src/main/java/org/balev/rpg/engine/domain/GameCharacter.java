package org.balev.rpg.engine.domain;

import org.balev.rpg.engine.domain.action.Action;
import org.balev.rpg.engine.domain.action.ActionsBuilder;

import java.util.List;

/**
 * Created by Aleksandr Balev on 20.07.2015.
 */
// TODO: make polymorphic - change MappingUtils and tests!
public class GameCharacter {

    private String name;
    private Integer life;
    private Integer experience;
    private Location location;

    public GameCharacter(String name, Integer initialLife) {
        this.name = name;
        this.life = initialLife;
        this.experience = 0;
    }

    public void gainExperience(Integer experience) {
        this.experience += experience;
    }

    public List<Action> explore() {
        return new ActionsBuilder()
                .withChangeLocationActions(location.getNearbyLocations(), this)
                .withFightActions(this, location.getGameCharacters())
                .build();
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Integer getLife() {
        return life;
    }

    public void kill() {
        life = 0;
    }

    public boolean isAlive() {
        return life > 0;
    }

    public Integer getExperience() {
        return experience;
    }

    @Override
    public String toString() {
        return "GameCharacter{" +
                "life=" + life +
                ", name='" + name + '\'' +
                ", experience=" + experience +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameCharacter that = (GameCharacter) o;

        if (experience != null ? !experience.equals(that.experience) : that.experience != null) return false;
        if (life != null ? !life.equals(that.life) : that.life != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (location == null || that.location == null ? true : !location.getName().equals(that.location.getName()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (experience != null ? experience.hashCode() : 0);
        result = 31 * result + (life != null ? life.hashCode() : 0);
        result = 31 * result + (location != null ? location.getName().hashCode() : 0);
        return result;
    }
}
