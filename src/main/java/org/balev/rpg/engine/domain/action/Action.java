package org.balev.rpg.engine.domain.action;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public interface Action {

    /**
     * There will be executed code related to some action in the game,
     */
    void execute();
}
