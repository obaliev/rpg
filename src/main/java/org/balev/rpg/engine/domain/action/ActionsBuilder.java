package org.balev.rpg.engine.domain.action;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by obaliev on 9/3/15.
 */
public class ActionsBuilder {

    private List<ChangeLocationAction> changeLocationActions;
    private List<FightAction> fightActions;

    public ActionsBuilder withChangeLocationActions(List<Location> locations, GameCharacter gameCharacter) {
        changeLocationActions = new ArrayList<>();

        locations.stream()
                .forEach(location -> changeLocationActions.add(new ChangeLocationAction(location, gameCharacter)));

        return this;
    }

    public ActionsBuilder withFightActions(GameCharacter gameCharacter, List<GameCharacter> gameCharacters) {
        fightActions = new ArrayList<>();

        gameCharacters.stream()
                .filter(currentGameCharacter -> currentGameCharacter.isAlive() && !currentGameCharacter.equals(gameCharacter))
                .forEach(currentGameCharacter
                        -> fightActions.add(new FightAction(gameCharacter, currentGameCharacter)));

        return this;
    }

    public List<Action> build() {
        List<Action> actions = new ArrayList<>();

        if (!changeLocationActions.isEmpty()) {
            actions.addAll(changeLocationActions);
        }

        if (!fightActions.isEmpty()) {
            actions.addAll(fightActions);
        }

        return actions;
    }

}
