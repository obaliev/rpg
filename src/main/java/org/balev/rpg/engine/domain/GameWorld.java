package org.balev.rpg.engine.domain;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Aleksandr Balev on 20.07.2015.
 */
public class GameWorld {

    private String name;
    private Map map;

    private GameWorld(String name, Map map) {
        this.name = name;
        this.map = map;
    }

    public List<Location> getMapLocations() {
        return map.getLocations();
    }

    public GameCharacter findGameCharacterByName(String gameCharacterName) {
        return map.findGameCharacterByName(gameCharacterName);
    }

    public String getName() {
        return name;
    }

    public Map getMap() {
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameWorld gameWorld = (GameWorld) o;

        if (map != null ? !map.equals(gameWorld.map) : gameWorld.map != null) return false;
        if (name != null ? !name.equals(gameWorld.name) : gameWorld.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (map != null ? map.hashCode() : 0);
        return result;
    }

    public static class GameWorldBuilder {

        private final Map map;
        private final String gameWorldName;

        public GameWorldBuilder(String gameWorldName) {
            this(gameWorldName, new Map());
        }

        public GameWorldBuilder(String gameWorldName, Map map) {
            this.gameWorldName = gameWorldName;
            this.map = map;
        }

        public GameWorldBuilder addLocation(Location location) {
            if (map.containsLocation(location)) {
                throw new IllegalArgumentException(String.format("Current Location '%s' is already exist on the map", location.getName()));
            }
            map.addLocation(location);

            return this;
        }

        public GameWorldBuilder markLocationsAsNearbyToEachOther(Location firstLocation, Location secondLocation) {
            if (!map.containsLocation(secondLocation)) {
                throw new IllegalArgumentException(String.format("Location '%s' must be exist on the map", secondLocation.getName()));
            }

            if (!map.containsLocation(firstLocation)) {
                throw new IllegalArgumentException(String.format("Location '%s' must be exist on the map", secondLocation.getName()));
            }

            map.markLocationsAsNearbyToEachOther(firstLocation, secondLocation);

            return this;
        }

        public GameWorldBuilder addNewLocationAndMarkLocationsAsNearbyToEachOther(Location newLocation, List<Location> existingLocations) {
            addLocation(newLocation);

            for (Location existingLocation : existingLocations) {
                markLocationsAsNearbyToEachOther(newLocation, existingLocation);
            }

            return this;
        }

        public GameWorldBuilder addNewLocationAndMarkItNearbyToExistingLocation(Location newLocation, Location existingLocation) {
            return addNewLocationAndMarkLocationsAsNearbyToEachOther(newLocation, Arrays.asList(existingLocation));
        }

        public Location findAddedLocationByName(String locationName) {
            return map.findLocationByName(locationName);
        }

        public GameWorld build() {
            if (map.getLocations().isEmpty()) {
                throw new IllegalStateException("Game World must have at least 1 location on the map");
            }

            return new GameWorld(gameWorldName, map);
        }
    }
}
