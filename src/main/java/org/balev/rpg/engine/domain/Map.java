package org.balev.rpg.engine.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class Map {

    private List<Location> locations;

    public Map() {
        locations = new ArrayList<Location>();
    }

    public GameCharacter findGameCharacterByName(String gameCharacterName) {
        for (Location location : locations) {
            Optional<GameCharacter> gameCharacter = location.getGameCharacterByNameIfExist(gameCharacterName);
            if (gameCharacter.isPresent()) {
                return gameCharacter.get();
            }
        }

        throw new IllegalArgumentException(String.format("No such game character with name '%s'", gameCharacterName));
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void addLocation(Location newLocation) {
        locations.add(newLocation);
    }

    public void markLocationsAsNearbyToEachOther(Location firstLocation, Location secondLocation) {
        firstLocation.addToNearbyLocations(secondLocation);
    }

    public boolean containsLocation(Location location) {
        return locations.contains(location);
    }

    public Location findLocationByName(String locationName) {
        return locations.stream().filter(location -> location.getName().equals(locationName)).findAny().get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Map map = (Map) o;

        if (locations != null ? !locations.equals(map.locations) : map.locations != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return locations != null ? locations.hashCode() : 0;
    }
}
