package org.balev.rpg.engine.domain.action;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.Location;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class ChangeLocationAction implements Action {

    private Location locationToChange;
    private GameCharacter gameCharacter;

    public ChangeLocationAction(Location locationToChange, GameCharacter gameCharacter) {
        this.locationToChange = locationToChange;
        this.gameCharacter = gameCharacter;
    }


    @Override
    public void execute() {
        gameCharacter.getLocation().removeGameCharacter(gameCharacter);
        locationToChange.putGameCharacter(gameCharacter);
    }

    @Override
    public String toString() {
        return String.format("Change location to '%s'", locationToChange.getName());
    }

    public GameCharacter getGameCharacter() {
        return gameCharacter;
    }
}
