package org.balev.rpg.engine.domain.action;

import org.balev.rpg.engine.domain.GameCharacter;

import java.util.Random;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class FightAction implements Action {

    private GameCharacter fightInitializerGameCharacter;
    private GameCharacter secondGameCharacter;

    public FightAction(GameCharacter fightInitializerGameCharacter, GameCharacter secondGameCharacter) {
        this.fightInitializerGameCharacter = fightInitializerGameCharacter;
        this.secondGameCharacter = secondGameCharacter;
    }

    @Override
    public void execute() {
        Integer firstGCStrength = fightInitializerGameCharacter.getLife() + fightInitializerGameCharacter.getExperience();
        Integer secondGCStrength = secondGameCharacter.getLife() + secondGameCharacter.getExperience();

        boolean firstGCWin = false;
        boolean secondGCWin = false;

        if (firstGCStrength > secondGCStrength) {
            firstGCWin = true;
        } else if (firstGCStrength < secondGCStrength) {
            secondGCWin = true;
        } else { // if firstGCStrength == secondGCStrength
            if (new Random().nextInt(2) == 0) {
                firstGCWin = true;
            } else {
                secondGCWin = true;
            }
        }

        if (firstGCWin) {
            secondGameCharacter.kill();
            fightInitializerGameCharacter.gainExperience(secondGameCharacter.getExperience() == 0 ? 1 : secondGameCharacter.getExperience());
        }

        if (secondGCWin) {
            fightInitializerGameCharacter.kill();
            secondGameCharacter.gainExperience(fightInitializerGameCharacter.getExperience() == 0 ? 1 : fightInitializerGameCharacter.getExperience());
        }
    }

    @Override
    public String toString() {
        return String.format("Fight with '%s' (life=%d)", secondGameCharacter.getName(), secondGameCharacter.getLife());
    }

    public GameCharacter getFightInitializerGameCharacter() {
        return fightInitializerGameCharacter;
    }

    public GameCharacter getSecondGameCharacter() {
        return secondGameCharacter;
    }
}
