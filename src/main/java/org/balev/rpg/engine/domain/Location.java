package org.balev.rpg.engine.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class Location {

    private final String name;

    private List<GameCharacter> gameCharacters;
    private List<Location> nearbyLocations;

    public Location(String name) {
        this.name = name;
        this.gameCharacters = new ArrayList<>();
        this.nearbyLocations = new ArrayList<>();
    }

    public boolean containsGameCharacter(GameCharacter gameCharacter) {
        return gameCharacters.contains(gameCharacter);
    }

    public boolean containsNearbyLocation(Location locationToCheck) {
        return nearbyLocations.contains(locationToCheck);
    }

    public String getName() {
        return name;
    }

    public List<GameCharacter> getGameCharacters() {
        // TODO: return a copy to make it safety
        return gameCharacters;
    }

    public List<Location> getNearbyLocations() {
        return nearbyLocations;
    }

    public void putGameCharacter(GameCharacter gameCharacter) {
        if (containsGameCharacter(gameCharacter)) {
            throw new IllegalArgumentException(String.format("Game Character '%s' is already exist on current location '%s'",
                    gameCharacter.getName(), getName()));
        }

        gameCharacter.setLocation(this);
        gameCharacters.add(gameCharacter);
    }

    public void removeGameCharacter(GameCharacter gameCharacter) {
        if (!containsGameCharacter(gameCharacter)) {
            throw new IllegalArgumentException(String.format("Game Character '%s' is not exist on current location '%s'",
                    gameCharacter.getName(), getName()));
        }

        gameCharacters.remove(gameCharacter);
        gameCharacter.setLocation(null);
    }

    /**
     * Set given location as nearby to this location. The method works in 2 side: <br />
     * Adds to nearbyLocations in both this and location objects
     *
     * @param location location which will be added to nearbyLocations
     */
    public void addToNearbyLocations(Location location) {
        if (location == this) {
            throw new IllegalArgumentException("You cannot add location itself to nearby locations");
        }

        if (containsNearbyLocation(location)) {
            throw new IllegalStateException(String.format("Location '%s' is already added as nearby to current '%s'", location.getName(), getName()));
        }

        nearbyLocations.add(location);
        location.addToNearbyLocationsAndExit(this);
    }

    private void addToNearbyLocationsAndExit(Location location) {
        if (containsNearbyLocation(location)) {
            throw new IllegalStateException(String.format("Location '%s' is already added as nearby to current '%s'", location.getName(), getName()));
        }

        nearbyLocations.add(location);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (gameCharacters != null ? !gameCharacters.equals(location.gameCharacters) : location.gameCharacters != null)
            return false;
        if (name != null ? !name.equals(location.name) : location.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (gameCharacters != null ? gameCharacters.hashCode() : 0);
        return result;
    }

    public Optional<GameCharacter> getGameCharacterByNameIfExist(String gameCharacterName) {
        return gameCharacters.stream().filter(gameCharacter -> gameCharacter.getName().equals(gameCharacterName)).findAny();
    }
}
