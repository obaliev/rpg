package org.balev.rpg.persistance.serialization.data;

import java.io.Serializable;

/**
 * Created by Aleksandr Balev on 26.07.2015.
 */
public class RPGDTO implements Serializable {

    private GameCharacterDTO mainGameCharacterDTO;
    private GameWorldDTO gameWorldDTO;

    public GameCharacterDTO getMainGameCharacterDTO() {
        return mainGameCharacterDTO;
    }

    public void setMainGameCharacterDTO(GameCharacterDTO mainGameCharacterDTO) {
        this.mainGameCharacterDTO = mainGameCharacterDTO;
    }

    public GameWorldDTO getGameWorldDTO() {
        return gameWorldDTO;
    }

    public void setGameWorldDTO(GameWorldDTO gameWorldDTO) {
        this.gameWorldDTO = gameWorldDTO;
    }
}
