package org.balev.rpg.persistance.serialization.data;

import java.io.Serializable;

/**
 * Created by Aleksandr Balev on 23.07.2015.
 */
public class GameWorldDTO implements Serializable {

    private String name;
    private MapDTO mapDTO;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MapDTO getMapDTO() {
        return mapDTO;
    }

    public void setMapDTO(MapDTO mapDTO) {
        this.mapDTO = mapDTO;
    }
}
