package org.balev.rpg.persistance.serialization.data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Aleksandr Balev on 23.07.2015.
 */
public class MapDTO implements Serializable {

    private List<LocationDTO> locationDTOs;

    public List<LocationDTO> getLocationDTOs() {
        return locationDTOs;
    }

    public void setLocationDTOs(List<LocationDTO> locationDTOs) {
        this.locationDTOs = locationDTOs;
    }
}
