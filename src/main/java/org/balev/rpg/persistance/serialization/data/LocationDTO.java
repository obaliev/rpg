package org.balev.rpg.persistance.serialization.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr Balev on 23.07.2015.
 */
public class LocationDTO implements Serializable {

    private String name;
    private List<GameCharacterDTO> gameCharacterDTOs;
    private List<LocationDTO> nearbyLocationDTOs = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GameCharacterDTO> getGameCharacterDTOs() {
        return gameCharacterDTOs;
    }

    public void setGameCharacterDTOs(List<GameCharacterDTO> gameCharacterDTOs) {
        this.gameCharacterDTOs = gameCharacterDTOs;
    }

    public List<LocationDTO> getNearbyLocationDTOs() {
        return nearbyLocationDTOs;
    }

    public void setNearbyLocationDTOs(List<LocationDTO> nearbyLocationDTOs) {
        this.nearbyLocationDTOs = nearbyLocationDTOs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocationDTO that = (LocationDTO) o;

        if (gameCharacterDTOs != null ? !gameCharacterDTOs.equals(that.gameCharacterDTOs) : that.gameCharacterDTOs != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (gameCharacterDTOs != null ? gameCharacterDTOs.hashCode() : 0);
        return result;
    }
}
