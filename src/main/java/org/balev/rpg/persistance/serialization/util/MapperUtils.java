package org.balev.rpg.persistance.serialization.util;

import org.balev.rpg.cli.domain.RPG;
import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.balev.rpg.persistance.serialization.data.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr Balev on 23.07.2015.
 */
public final class MapperUtils {

    private MapperUtils() {
    }

    public static RPGDTO map(RPG rpg) {
        RPGDTO rpgdto = new RPGDTO();

        GameWorldDTO gameWorldDTO = map(rpg.getGameWorld());
        rpgdto.setGameWorldDTO(gameWorldDTO);

        rpgdto.setMainGameCharacterDTO(
                findGameCharacterDTOByNameThroughLocationDTOs(
                        gameWorldDTO.getMapDTO().getLocationDTOs(),
                        rpg.getMainGameCharacter().getName()));

        return rpgdto;
    }

    private static GameCharacterDTO findGameCharacterDTOByNameThroughLocationDTOs(List<LocationDTO> locationDTOs, String gameCharacterName) {
        for (LocationDTO locationDTO : locationDTOs) {
            for (GameCharacterDTO gameCharacterDTO : locationDTO.getGameCharacterDTOs()) {
                if (gameCharacterDTO.getName().equals(gameCharacterName)) {
                    return gameCharacterDTO;
                }
            }
        }

        throw new IllegalArgumentException("No found on locations such game character dto with name " + gameCharacterName);
    }

    public static RPG map(RPGDTO rpgdto) {
        GameWorld gameWorld = map(rpgdto.getGameWorldDTO());

        RPG rpg = new RPG(gameWorld);
        rpg.setMainGameCharacter(rpg.getGameWorld().findGameCharacterByName(rpgdto.getMainGameCharacterDTO().getName()));

        return rpg;
    }

    public static GameCharacterDTO map(GameCharacter gameCharacter) {
        GameCharacterDTO gameCharacterDTO = new GameCharacterDTO();

        gameCharacterDTO.setName(gameCharacter.getName());
        gameCharacterDTO.setLife(gameCharacter.getLife());
        gameCharacterDTO.setExperience(gameCharacter.getExperience());

        return gameCharacterDTO;
    }

    public static GameCharacter map(GameCharacterDTO gameCharacterDTO) {
        GameCharacter gameCharacter = new GameCharacter(gameCharacterDTO.getName(), gameCharacterDTO.getLife());
        gameCharacter.gainExperience(gameCharacterDTO.getExperience());

        return gameCharacter;
    }

    public static GameWorldDTO map(GameWorld gameWorld) {
        List<LocationDTO> locationDTOs = new ArrayList<LocationDTO>(gameWorld.getMapLocations().size());

        MapDTO mapDTO = new MapDTO();
        mapDTO.setLocationDTOs(locationDTOs);

        for (Location location : gameWorld.getMapLocations()) {
            locationDTOs.add(map(location));
        }

        for (Location location : gameWorld.getMapLocations()) {
            LocationDTO locationDTO = map(location);

            for (Location nearbyLocation : location.getNearbyLocations()) {
                LocationDTO nearbyLocationDTO = findLocationDTOByName(mapDTO, nearbyLocation.getName());

                if (!isNearbyLocationDTOAlreadyAddedToLocationDTO(nearbyLocationDTO, locationDTO)) {
                    locationDTO.getNearbyLocationDTOs().add(nearbyLocationDTO);
                    nearbyLocationDTO.getNearbyLocationDTOs().add(locationDTO);
                }
            }
        }

        GameWorldDTO gameWorldDTO = new GameWorldDTO();
        gameWorldDTO.setName(gameWorld.getName());
        gameWorldDTO.setMapDTO(mapDTO);

        return gameWorldDTO;
    }

    private static boolean isNearbyLocationDTOAlreadyAddedToLocationDTO(LocationDTO nearbyLocationDTOToCheck, LocationDTO locationDTO) {
        return locationDTO.getNearbyLocationDTOs().contains(nearbyLocationDTOToCheck);
    }

    private static LocationDTO findLocationDTOByName(MapDTO mapDTO, String locationDTOName) {
        return mapDTO.getLocationDTOs().stream()
                .filter(locationDTO -> locationDTO.getName().equals(locationDTOName)).findAny().get();
    }

    public static LocationDTO map(Location location) {
        LocationDTO locationDTO = new LocationDTO();

        locationDTO.setName(location.getName());

        locationDTO.setGameCharacterDTOs(new ArrayList<GameCharacterDTO>());
        for (GameCharacter gameCharacter : location.getGameCharacters()) {
            GameCharacterDTO gameCharacterDTO = map(gameCharacter);
            gameCharacterDTO.setCurrentLocationDTO(locationDTO);

            locationDTO.getGameCharacterDTOs().add(gameCharacterDTO);
        }

        return locationDTO;
    }

    public static GameWorld map(GameWorldDTO gameWorldDTO) {
        GameWorld.GameWorldBuilder gameWorldBuilder = new GameWorld.GameWorldBuilder(gameWorldDTO.getName());

        for (LocationDTO locationDTO : gameWorldDTO.getMapDTO().getLocationDTOs()) {
            Location location = map(locationDTO);

            gameWorldBuilder.addLocation(location);
        }

        // Update nearby locations
        for (LocationDTO locationDTO : gameWorldDTO.getMapDTO().getLocationDTOs()) {
            Location location = gameWorldBuilder.findAddedLocationByName(locationDTO.getName());

            for (LocationDTO nearbyLocationDTO : locationDTO.getNearbyLocationDTOs()) {
                Location nearbyLocation = gameWorldBuilder.findAddedLocationByName(nearbyLocationDTO.getName());

                if (!location.containsNearbyLocation(nearbyLocation)) {
                    gameWorldBuilder.markLocationsAsNearbyToEachOther(location, nearbyLocation);
                }
            }
        }

        return gameWorldBuilder.build();
    }

    public static Location map(LocationDTO locationDTO) {
        Location location = new Location(locationDTO.getName());

        for (GameCharacterDTO gameCharacterDTO : locationDTO.getGameCharacterDTOs()) {
            // TODO: Handle it to be not generic after deserealization, maybe when serialize add a type in future...!!!
            GameCharacter gameCharacter = map(gameCharacterDTO);
            gameCharacter.setLocation(location);

            location.putGameCharacter(gameCharacter);
        }

        return location;
    }
}
