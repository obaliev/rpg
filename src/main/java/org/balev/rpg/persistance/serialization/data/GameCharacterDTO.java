package org.balev.rpg.persistance.serialization.data;

import java.io.Serializable;

/**
 * Created by Aleksandr Balev on 23.07.2015.
 */
public class GameCharacterDTO implements Serializable {

    private String name;
    private Integer life;
    private Integer experience;
    private LocationDTO currentLocationDTO;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLife() {
        return life;
    }

    public void setLife(Integer life) {
        this.life = life;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public LocationDTO getCurrentLocationDTO() {
        return currentLocationDTO;
    }

    public void setCurrentLocationDTO(LocationDTO currentLocationDTO) {
        this.currentLocationDTO = currentLocationDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameCharacterDTO that = (GameCharacterDTO) o;

        if (experience != null ? !experience.equals(that.experience) : that.experience != null) return false;
        if (life != null ? !life.equals(that.life) : that.life != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (currentLocationDTO == null || that.currentLocationDTO == null ? true : !currentLocationDTO.getName().equals(that.currentLocationDTO.getName()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (life != null ? life.hashCode() : 0);
        result = 31 * result + (experience != null ? experience.hashCode() : 0);
        result = 31 * result + (currentLocationDTO != null ? currentLocationDTO.getName().hashCode() : 0);
        return result;
    }
}
