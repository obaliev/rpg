package org.balev.rpg.persistance;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.balev.rpg.cli.domain.RPG;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.persistance.serialization.data.GameWorldDTO;
import org.balev.rpg.persistance.serialization.data.RPGDTO;
import org.balev.rpg.persistance.serialization.util.MapperUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Aleksandr Balev on 23.07.2015.
 */
public class PersistenceService {

    public static final String SAVE_FILE_PREFIX = "RPG_";

    private String persistencePath;

    public PersistenceService(String persistencePath) {
        File saveDir = new File(persistencePath);

        if (!saveDir.exists()) {
            try {
                FileUtils.forceMkdir(saveDir);
            } catch (IOException e) {
                throw new IllegalStateException(
                        String.format("Cannot create directory for save games '%s'", persistencePath), e);
            }
        }

        this.persistencePath = persistencePath;
    }

    public void saveGameWorld(GameWorld gameWorld) {
        try {
            SerializationUtils.serialize(
                    MapperUtils.map(gameWorld),
                    new FileOutputStream(
                            new File(getFullPathWithPrefix() + (new Date().getTime()))));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to save game world", e);
        }
    }

    public List<Date> findSavedFileDates() {
        File[] savedFiles = new File(persistencePath).listFiles();
        List<Date> savedFileDates = new ArrayList<>(savedFiles.length);

        for (File savedFile : savedFiles) {
            if (savedFile.getName().contains(SAVE_FILE_PREFIX)) {
                long time = NumberUtils.toLong(StringUtils.substringAfter(savedFile.getName(), SAVE_FILE_PREFIX));
                savedFileDates.add(new Date(time));
            }
        }

        return savedFileDates;
    }

    public GameWorld resumeSavedGame(Date savedDate) {
        File savedFile = new File(getFullPathWithPrefix() + savedDate.getTime());
        GameWorldDTO gameWorldDTO;

        try {
            gameWorldDTO = SerializationUtils.deserialize(new FileInputStream(savedFile));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Not found save data for " + savedDate);
        }

        return MapperUtils.map(gameWorldDTO);
    }

    private String getFullPathWithPrefix() {
        return persistencePath + File.separator + SAVE_FILE_PREFIX;
    }

    public void saveRPG(RPG rpg) {
        try {
            SerializationUtils.serialize(
                    MapperUtils.map(rpg),
                    new FileOutputStream(
                            new File(getFullPathWithPrefix() + (new Date().getTime()))));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to save rpg", e);
        }
    }

    public RPG resumeRPG(Date savedDate) {
        File savedFile = new File(getFullPathWithPrefix() + savedDate.getTime());
        RPGDTO rpgdto;

        try {
            rpgdto = SerializationUtils.deserialize(new FileInputStream(savedFile));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Not found save data for " + savedDate);
        }

        return MapperUtils.map(rpgdto);
    }

    public String getPersistencePath() {
        return persistencePath;
    }
}
