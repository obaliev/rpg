package org.balev.rpg;

import org.balev.rpg.cli.ConsoleApplicationManager;
import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;
import org.balev.rpg.cli.util.CLIUtils;
import org.balev.rpg.domain.GameMaster;
import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.Location;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 * Created by Aleksandr Balev on 25.07.2015.
 */
public class Main {

    public static void main(String[] args) {

        try {
            GameMaster gameMaster = new GameMaster();

            buildJVWGameWorldWithGameMaster(gameMaster);
            GameMasterManager.initGameMaster(gameMaster);

            ConsoleApplicationManager.setActiveView(StaticViewHolder.gameMainMenuView);

            while (true) {
                CLIUtils.clr();
                CLIUtils.print(ConsoleApplicationManager.getActiveView().getState());
                ConsoleApplicationManager.getActiveView().updateState(CLIUtils.getReader().readLine());
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Save the data if crash...
        }
    }

    /**
     * Build Java Virtual World
     *
     * @param gameMaster some kind of God for RPG :)
     * @throws IOException
     */
    private static void buildJVWGameWorldWithGameMaster(GameMaster gameMaster) throws IOException {
        gameMaster.startCreatingNewGameWorld("Java Virtual World");

        // Create world locations
        Location mainThreadLocation = gameMaster.createLocation("Main Thread");
        Location mainSubThread1Location = gameMaster.createLocation("Thread #1");
        Location mainSubThread2Location = gameMaster.createLocation("Thread #2");
        Location mainSubThread3Location = gameMaster.createLocation("Thread #3");

        Location garbageCollectorLocation = gameMaster.createLocation("Garbage Collector");
        Location heapLocation = gameMaster.createLocation("Heap");
        Location stackLocation = gameMaster.createLocation("Stack");
        Location permGemLocation = gameMaster.createLocation("PermGem");

        gameMaster.bindLocations(mainSubThread1Location, mainThreadLocation);
        gameMaster.bindLocations(mainSubThread2Location, mainThreadLocation);
        gameMaster.bindLocations(mainSubThread3Location, mainThreadLocation);

        gameMaster.bindLocations(garbageCollectorLocation, mainThreadLocation);
        gameMaster.bindLocations(heapLocation, garbageCollectorLocation);
        gameMaster.bindLocations(stackLocation, heapLocation);
        gameMaster.bindLocations(permGemLocation, garbageCollectorLocation);

        // Create monsters :)
        gameMaster.putGameCharacterToLocation(gameMaster.createGameCharacter("OutOfMemoryError: Heap", 20), heapLocation);
        gameMaster.putGameCharacterToLocation(gameMaster.createGameCharacter("OutOfMemoryError: StackOverflow", 20), stackLocation);
        gameMaster.putGameCharacterToLocation(gameMaster.createGameCharacter("OutOfMemoryError: PermGem", 20), permGemLocation);

        gameMaster.putGameCharacterToLocation(gameMaster.createGameCharacter("Thread #1 Deadlock", 5), mainSubThread1Location);
        gameMaster.putGameCharacterToLocation(gameMaster.createGameCharacter("Thread #2 Deadlock", 10), mainSubThread2Location);
        gameMaster.putGameCharacterToLocation(gameMaster.createGameCharacter("Thread #3 Deadlock", 15), mainSubThread3Location);

        // Create small monsters to Kill and gain experience for better life
        List<Location> allLocations = new ArrayList<>();
        allLocations.addAll(Arrays.asList(
                mainSubThread1Location,
                mainSubThread1Location, mainSubThread2Location, mainSubThread3Location,
                garbageCollectorLocation,
                heapLocation, stackLocation, permGemLocation));
        for (Location location : allLocations) {
            int randomWeakMonstersCount = new Random().nextInt(4);

            for (int i = 0; i < randomWeakMonstersCount; i++) {
                int lifeValue = new Random().nextInt(3) + 1;
                gameMaster.putGameCharacterToLocation(
                        new GameCharacter(String.format("%s Monster #%d", location.getName(), i), lifeValue),
                        location);
            }
        }

        gameMaster.buildGameWorld();
    }


}
