package org.balev.rpg.cli.domain;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;

/**
 * Created by Aleksandr Balev on 25.07.2015.
 */
public class RPG {

    private GameWorld gameWorld;
    private GameCharacter mainGameCharacter;

    public RPG(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public GameCharacter getMainGameCharacter() {
        return mainGameCharacter;
    }

    public void setMainGameCharacter(GameCharacter mainGameCharacter) {
        this.mainGameCharacter = mainGameCharacter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RPG rpg = (RPG) o;

        if (gameWorld != null ? !gameWorld.equals(rpg.gameWorld) : rpg.gameWorld != null) return false;
        if (mainGameCharacter != null ? !mainGameCharacter.equals(rpg.mainGameCharacter) : rpg.mainGameCharacter != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = gameWorld != null ? gameWorld.hashCode() : 0;
        result = 31 * result + (mainGameCharacter != null ? mainGameCharacter.hashCode() : 0);
        return result;
    }
}
