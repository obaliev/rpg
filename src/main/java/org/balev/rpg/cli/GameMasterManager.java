package org.balev.rpg.cli;

import org.balev.rpg.domain.GameMaster;
import org.balev.rpg.domain.GamePlayer;
import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;

/**
 * Created by obaliev on 9/20/15.
 */
public final class GameMasterManager {

    private static GameMaster gameMaster;

    public static void initGameMaster(GameMaster newGameMaster) {
        gameMaster = newGameMaster;
    }

    public static GameWorld getGameWorld() {
        return gameMaster.getGameWorld();
    }

    public static void setGameWorld(GameWorld gameWorld) {
        gameMaster.startCreatingNewGameWorld(gameWorld.getName(), gameWorld.getMap());
        gameMaster.buildGameWorld();
    }

    public static GameCharacter getMainGameCharacter() {
        return gameMaster.getMainGameCharacter();
    }

    public static void setMainGameCharacter(GameCharacter mainGameCharacter) {
        GamePlayer gamePlayer = new GamePlayer(mainGameCharacter);
        gameMaster.createStory();
        gameMaster.addGamePlayer(gamePlayer);
    }
}
