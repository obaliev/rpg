package org.balev.rpg.cli.mvp.view.dynamic;

import org.balev.rpg.cli.mvp.presenter.Presenter;

/**
 * Created by obaliev on 9/20/15.
 */
public class CharacterInfoDynamicView extends DynamicView {

    public CharacterInfoDynamicView(Presenter presenter) {
        super(presenter);
    }

    @Override
    public void initState() {
        state = "(Press Enter to Back to Game Menu)";
    }
}
