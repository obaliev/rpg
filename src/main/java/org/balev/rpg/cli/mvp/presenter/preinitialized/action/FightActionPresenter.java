package org.balev.rpg.cli.mvp.presenter.preinitialized.action;

import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.mvp.presenter.PresenterHolder;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.factory.ViewFactory;
import org.balev.rpg.engine.domain.action.Action;
import org.balev.rpg.engine.domain.action.FightAction;

/**
 * Created by obaliev on 9/21/15.
 */
public class FightActionPresenter extends ActionPresenter {

    int enemyLifeValueBeforeFighting;

    public FightActionPresenter(Action action) {
        super(action);
        this.enemyLifeValueBeforeFighting = ((FightAction) action).getSecondGameCharacter().getLife();
    }

    @Override
    protected View routeToView(String selectedValue) {
        View routedView = view;
        String enemyName = ((FightAction) action).getSecondGameCharacter().getName();

        if (GameMasterManager.getMainGameCharacter().isAlive()) {
            if (enemyLifeValueBeforeFighting >= 5) {
                routedView = ViewFactory.getInstance().createStaticView(
                        PresenterHolder.executedActionPresenter,
                        String.format("Great!!! You now beat '%s'! Now your Java Virtual World much stable!\n" +
                                "(Press Enter to continue)", enemyName));
            } else {
                routedView = ViewFactory.getInstance().createStaticView(
                        PresenterHolder.executedActionPresenter,
                        String.format("Good! You now have fought with '%s' and win!\n" +
                                "(Press Enter to continue)", enemyName));
            }
        } else {
            routedView = ViewFactory.getInstance().createStaticView(
                    PresenterHolder.gameOverPresenter,
                    String.format("Sorry, but your Java Virtual World is crashed with '%s' :(\n" +
                            "You can try to resume previously saved game or start new one\n" +
                            "(Press Enter to continue)", enemyName));
        }

        return routedView;
    }
}
