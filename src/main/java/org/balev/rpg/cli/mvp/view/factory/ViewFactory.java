package org.balev.rpg.cli.mvp.view.factory;

import org.balev.rpg.cli.mvp.presenter.Presenter;
import org.balev.rpg.cli.mvp.presenter.preinitialized.PreInitializedPresenter;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicView;
import org.balev.rpg.cli.mvp.view.static_.StaticView;

/**
 * Helps to instantiate views
 *
 * Created by obaliev on 9/22/15.
 */
public class ViewFactory {
    /**
     *
     */
    private static ViewFactory instance = new ViewFactory();

    private ViewFactory() {
    }

    public static ViewFactory getInstance() {
        return instance;
    }

    public StaticView createStaticView(final Presenter presenter, final String statevalue) {
        return new StaticView(presenter) {
            @Override
            public void initState() {
                state = statevalue;
            }
        };
    }

    public DynamicView createDynamicView(
            final PreInitializedPresenter preinitializedpresenter,
            final String statevalue) {
        return new DynamicView(preinitializedpresenter) {
            @Override
            public void initState() {
                state = statevalue;
            }
        };
    }
}
