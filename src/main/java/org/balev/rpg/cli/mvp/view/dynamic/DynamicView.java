package org.balev.rpg.cli.mvp.view.dynamic;

import org.balev.rpg.cli.mvp.presenter.Presenter;
import org.balev.rpg.cli.mvp.presenter.preinitialized.PreInitializedPresenter;
import org.balev.rpg.cli.mvp.view.View;

/**
 * Created by obaliev on 9/20/15.
 */
public abstract class DynamicView extends View {

    public DynamicView(Presenter presenter) {
        super(presenter);

        if (!(presenter instanceof PreInitializedPresenter)) {
            throw new IllegalArgumentException("Dynamic view must use PreInitializedPresenter");
        }
    }

    public final void initializeView() {
        initState();
        ((PreInitializedPresenter) presenter).init();
    }

    public final void setState(String state) {
        this.state = state;
    }
}
