package org.balev.rpg.cli.mvp.presenter;

import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.domain.RPG;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;
import org.balev.rpg.persistance.PersistenceService;

/**
 * Created by obaliev on 9/20/15.
 */
public class SaveGamePresenter extends Presenter {

    // TODO: make some common place where to store/get this services
    private PersistenceService persistenceService = new PersistenceService("/tmp");

    @Override
    protected void updateModel(String selectedValue) {
        RPG rpg = new RPG(GameMasterManager.getGameWorld());
        rpg.setMainGameCharacter(GameMasterManager.getMainGameCharacter());

        persistenceService.saveRPG(rpg);
    }

    @Override
    protected View routeToView(String selectedValue) {
        return StaticViewHolder.gameSuccessfullySavedGameMenuView;
    }
}
