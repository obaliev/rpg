package org.balev.rpg.cli.mvp.presenter.passive;

import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicViewHolder;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;

/**
 * Created by obaliev on 9/20/15.
 */
public class MainMenuPresenter extends RedirectPresenter {
    @Override
    protected View routeToView(String selectedValue) {
        switch (selectedValue) {
            case "1": // Start new game
                return StaticViewHolder.createCharacterView;
            case "2": // Resume previous game
                return DynamicViewHolder.resumeJVWView;
            case "3": // Exit
                return StaticViewHolder.byeView;
            default:
                return this.view;
        }
    }
}
