package org.balev.rpg.cli.mvp.presenter.passive;

import org.balev.rpg.cli.mvp.presenter.Presenter;

/**
 * <p>Special Presenter abstraction without updating model.
 * Usefull when you need to show view to the user and after user will update view state,
 * he will be redirected to proper presenter</p>
 *
 * Created by obaliev on 9/22/15.
 */
public abstract class RedirectPresenter extends Presenter {

    @Override
    protected final void updateModel(String selectedValue) {
        // NOP
    }
}
