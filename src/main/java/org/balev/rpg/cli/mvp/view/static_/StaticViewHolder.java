package org.balev.rpg.cli.mvp.view.static_;

import org.balev.rpg.cli.mvp.presenter.PresenterHolder;
import org.balev.rpg.cli.mvp.view.View;

/**
 * Created by obaliev on 9/20/15.
 */
public class StaticViewHolder {
    public static View gameMainMenuView = new StaticView(PresenterHolder.mainMenuPresenter) {
        @Override
        public void initState() {
            state = "Welcome to Java Virtual World (JVW)!!!\n" +
                    "Here you can try yourself and save your Java Virtual World.\n" +
                    "-------------------------------------\n" +
                    "Main menu (please type and Enter appropriate menu number)\n" +
                    "1. Start New JVW!\n" +
                    "2. Resume previous JVW!\n" +
                    "3. Say Bye...\n";
        }
    };
    public static View byeView = new StaticView(PresenterHolder.byePresenter) {
        @Override
        public void initState() {
            state = "Thank you for your time. See you, bye! (For exit just type Enter)\n";
        }
    };
    public static View createCharacterView = new StaticView(PresenterHolder.startNewJVWPresenter) {
        @Override
        public void initState() {
            state = "You're starting new Java Virtual World. Before please create your character.\n" +
                    "Your character's name> ";
        }
    };
    public static View gameMenuView = new StaticView(PresenterHolder.gameMenuPresenter) {
        @Override
        public void initState() {
            state = "You can: \n" + getGameMenuOptions();
        }
    };
    public static View resumedGameMenuView = new StaticView(PresenterHolder.gameMenuPresenter) {
        @Override
        public void initState() {
            state = "Java Virtual World has been loaded\n-------------------------------------\nYou can: \n" +
                    getGameMenuOptions();
        }
    };
    public static View startedNewGameMenuView = new StaticView(PresenterHolder.gameMenuPresenter) {
        @Override
        public void initState() {
            state = "Now you are in the Virtual World of Java and you can discover this world:\nSo you can try to:\n" +
                    getGameMenuOptions();
        }
    };
    public static View gameSuccessfullySavedGameMenuView = new StaticView(PresenterHolder.gameMenuPresenter) {
        @Override
        public void initState() {
            state = "Your game has been successfully saved.\nYou can:\n" +
                    getGameMenuOptions();
        }
    };
    public static View saveGameView = new StaticView(PresenterHolder.saveGamePresenter) {
        @Override
        public void initState() {
            // TODO Define some global paramteres holder to store temp directory and access such parameters
            state = "This game will be saved in '/tmp' directory\n(Press Enter to continue)\n";
        }
    };

    private static String getGameMenuOptions() {
        return "1. Explore this location\n" +
                "2. Show your character info\n" +
                "3. Save Game\n" +
                "4. Exit To Main Menu\n";
    }
}
