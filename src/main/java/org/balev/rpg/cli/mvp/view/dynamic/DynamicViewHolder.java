package org.balev.rpg.cli.mvp.view.dynamic;

import org.balev.rpg.cli.mvp.presenter.PresenterHolder;

/**
 * Created by obaliev on 9/20/15.
 */
public class DynamicViewHolder {

    public static DynamicView resumeJVWView = new ResumeJVWDynamicView(PresenterHolder.resumeJVWPresenter);
    public static DynamicView exploreActionsView = new ExploreActionsDynamicView(PresenterHolder.exploreActionsFacadePresenter);
    public static DynamicView characterInfoView = new CharacterInfoDynamicView(PresenterHolder.characterInfoPresenter);

    private DynamicViewHolder() {
    }

}
