package org.balev.rpg.cli.mvp.view.dynamic;

import org.balev.rpg.cli.mvp.presenter.Presenter;

/**
 * Created by obaliev on 9/20/15.
 */
public class ExploreActionsDynamicView extends DynamicView {

    public ExploreActionsDynamicView(Presenter presenter) {
        super(presenter);
    }

    @Override
    public void initState() {
        state = "Please choose the action:\n";
    }
}
