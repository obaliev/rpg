package org.balev.rpg.cli.mvp.presenter;

import org.balev.rpg.cli.mvp.presenter.passive.ExecutedActionPresenter;
import org.balev.rpg.cli.mvp.presenter.passive.GameMenuPresenter;
import org.balev.rpg.cli.mvp.presenter.passive.GameOverPresenter;
import org.balev.rpg.cli.mvp.presenter.passive.MainMenuPresenter;
import org.balev.rpg.cli.mvp.presenter.preinitialized.CharacterInfoPresenter;
import org.balev.rpg.cli.mvp.presenter.preinitialized.ResumeJVWPresenter;
import org.balev.rpg.cli.mvp.presenter.preinitialized.action.ExploreActionsFacadePresenter;

/**
 * Created by obaliev on 9/20/15.
 */
public class PresenterHolder {
    public static Presenter mainMenuPresenter = new MainMenuPresenter();
    public static Presenter startNewJVWPresenter = new StartNewJVWPresenter();
    public static Presenter exploreActionsFacadePresenter = new ExploreActionsFacadePresenter();
    public static Presenter gameMenuPresenter = new GameMenuPresenter();
    public static Presenter resumeJVWPresenter = new ResumeJVWPresenter();
    public static Presenter byePresenter = new ByePresenter();
    public static Presenter saveGamePresenter = new SaveGamePresenter();
    public static Presenter characterInfoPresenter = new CharacterInfoPresenter();
    public static Presenter executedActionPresenter = new ExecutedActionPresenter();
    public static Presenter gameOverPresenter = new GameOverPresenter();
}
