/**
 * Created by obaliev on 9/22/15.
 * <p>
 * <p>This package provides Model-View-Presenter implementation for console application</p>
 */
package org.balev.rpg.cli.mvp;