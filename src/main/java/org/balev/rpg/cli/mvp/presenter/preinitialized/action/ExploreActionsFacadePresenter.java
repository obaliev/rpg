package org.balev.rpg.cli.mvp.presenter.preinitialized.action;

import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.mvp.presenter.preinitialized.PreInitializedPresenter;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicView;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;
import org.balev.rpg.engine.domain.action.Action;
import org.balev.rpg.engine.util.ActionUtils;

import java.util.List;

/**
 * Created by obaliev on 9/20/15.
 */
public class ExploreActionsFacadePresenter extends PreInitializedPresenter {

    private List<Action> availableActions;
    private int selectedActionNum = 0;
    private Action selectedAction;
    private ActionPresenter actionPresenter;

    @Override
    public void init() {
        availableActions = GameMasterManager.getMainGameCharacter().explore();

        StringBuilder stringBuilder = new StringBuilder(view.getState());
        int i = 0;
        for (; i < availableActions.size(); i++) {
            stringBuilder.append(String.format("%d. %s\n", i + 1, availableActions.get(i).toString()));
        }
        stringBuilder.append(String.format("%d. Back to main menu\n", i + 1));

        ((DynamicView) view).setState(stringBuilder.toString());
    }

    @Override
    protected void updateModel(String selectedValue) {
        selectedActionNum = parseStringToInteger(selectedValue);

        if (selectedActionNum > 0 && selectedActionNum <= availableActions.size()) {
            selectedAction = availableActions.get(selectedActionNum - 1);

            actionPresenter = findActionPresenter(selectedAction);
            actionPresenter.updateModel(selectedValue);
        }
    }

    private ActionPresenter findActionPresenter(Action action) {
        if (action == null) {
            throw new IllegalArgumentException("Action cannot be null");
        }

        ActionPresenter presenter = null;

        if (ActionUtils.isFightAction(action)) {
            presenter = new FightActionPresenter(action);
        }

        if (ActionUtils.isChangeLocationAction(action)) {
            presenter = new ChangeLocationActionPresenter(action);
        }

        if (presenter == null) {
            throw new IllegalStateException("Unable to define proper presenter for action: " + action);
        }

        return presenter;
    }

    @Override
    protected View routeToView(String selectedValue) {
        View routedView = view;

        if (actionPresenter != null) {
            routedView = actionPresenter.routeToView(selectedValue);
        }

        if (selectedActionNum == availableActions.size() + 1) {
            routedView = StaticViewHolder.gameMenuView;
        }

        return routedView;
    }
}
