package org.balev.rpg.cli.mvp.presenter;

import org.balev.rpg.cli.mvp.view.View;

/**
 * Created by obaliev on 9/20/15.
 */
public class ByePresenter extends Presenter {

    @Override
    public void updateModel(String selectedValue) {
        System.exit(0);
    }

    @Override
    protected View routeToView(String selectedValue) {
        return null;
    }
}