package org.balev.rpg.cli.mvp.presenter.preinitialized;

import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.domain.RPG;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicView;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;
import org.balev.rpg.persistance.PersistenceService;

import java.util.Date;
import java.util.List;

/**
 * Created by obaliev on 9/20/15.
 */
public class ResumeJVWPresenter extends PreInitializedPresenter {

    private PersistenceService persistenceService = new PersistenceService("/tmp");
    private List<Date> savedDates;
    private RPG loadedRPG;
    private int selectedDateNum = -1;

    @Override
    public void init() {
        savedDates = persistenceService.findSavedFileDates();

        StringBuilder stringBuilder = new StringBuilder(view.getState());
        int i = 0;
        for (; i < savedDates.size(); i++) {
            stringBuilder.append(String.format("%d. %s\n", i + 1, savedDates.get(i).toString()));
        }
        stringBuilder.append(String.format("%d. Back to main menu\n", i + 1));

        ((DynamicView) view).setState(stringBuilder.toString());
    }

    @Override
    public void updateModel(String selectedValue) {
        selectedDateNum = parseStringToInteger(selectedValue);

        if (selectedDateNum > 0 && selectedDateNum <= savedDates.size()) {
            loadedRPG = persistenceService.resumeRPG(savedDates.get(selectedDateNum - 1));

            GameMasterManager.setGameWorld(loadedRPG.getGameWorld());
            GameMasterManager.setMainGameCharacter(loadedRPG.getMainGameCharacter());
        }
    }

    @Override
    protected View routeToView(String selectedValue) {
        View routedView = view;

        if (loadedRPG != null && selectedDateNum != -1) {
            routedView = StaticViewHolder.resumedGameMenuView;
        }

        // All date variants + 1 = Back To Main menu
        if (selectedDateNum == savedDates.size() + 1) {
            routedView = StaticViewHolder.gameMainMenuView;
        }

        return routedView;
    }
}
