package org.balev.rpg.cli.mvp.view.dynamic;

import org.balev.rpg.cli.mvp.presenter.Presenter;

/**
 * Created by obaliev on 9/20/15.
 */
public class ResumeJVWDynamicView extends DynamicView {

    public ResumeJVWDynamicView(Presenter presenter) {
        super(presenter);
    }

    @Override
    public void initState() {
        state = "Please select date of saved game:\n";
    }
}
