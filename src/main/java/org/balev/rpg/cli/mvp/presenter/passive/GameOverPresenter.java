package org.balev.rpg.cli.mvp.presenter.passive;

import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;

/**
 * Created by obaliev on 9/20/15.
 */
public class GameOverPresenter extends RedirectPresenter {

    @Override
    protected View routeToView(String selectedValue) {
        return StaticViewHolder.gameMainMenuView;
    }
}
