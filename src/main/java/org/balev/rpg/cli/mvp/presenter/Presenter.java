package org.balev.rpg.cli.mvp.presenter;

import org.balev.rpg.cli.ConsoleApplicationManager;
import org.balev.rpg.cli.mvp.view.View;

/**
 * Created by obaliev on 9/20/15.
 */
public abstract class Presenter {
    protected View view;

    /**
     * @param string simple string
     * @return parsed integer if string is just an int, or -1 otherwise
     */
    protected static final int parseStringToInteger(String string) {
        int parsedInt;

        try {
            parsedInt = Integer.valueOf(string);
        } catch (NumberFormatException e) {
            parsedInt = -1;
        }

        return parsedInt;
    }

    public void setView(View view) {
        this.view = view;
    }

    public final void updateModelAndView(String selectedValue) {
        if (validate(selectedValue)) {
            updateModel(selectedValue);
            ConsoleApplicationManager.setActiveView(routeToView(selectedValue));
        } else {
            ConsoleApplicationManager.setActiveView(view);
        }
    }

    protected boolean validate(String selectedValue) {
        return true;
    }

    /**
     * Method responsible for updating model
     *
     * @param selectedValue
     */
    protected abstract void updateModel(String selectedValue);

    /**
     * Method responsible for redirection to view after model updating
     *
     * @param selectedValue
     * @return
     */
    protected abstract View routeToView(String selectedValue);
}
