package org.balev.rpg.cli.mvp.presenter.passive;

import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicViewHolder;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;

/**
 * Created by obaliev on 9/20/15.
 */
public class GameMenuPresenter extends RedirectPresenter {
    @Override
    protected View routeToView(String selectedValue) {
        switch (selectedValue) {
            case "1": // Explore
                return DynamicViewHolder.exploreActionsView;
            case "2": // Show Character Info
                return DynamicViewHolder.characterInfoView;
            case "3": // Save Game
                return StaticViewHolder.saveGameView;
            case "4": // Exit to main menu
                return StaticViewHolder.gameMainMenuView;
            default:
                return view;
        }
    }
}
