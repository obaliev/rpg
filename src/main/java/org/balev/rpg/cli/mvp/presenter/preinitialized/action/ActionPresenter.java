package org.balev.rpg.cli.mvp.presenter.preinitialized.action;

import org.balev.rpg.cli.mvp.presenter.preinitialized.PreInitializedPresenter;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.engine.domain.action.Action;

/**
 * Created by obaliev on 9/22/15.
 */
public abstract class ActionPresenter extends PreInitializedPresenter {

    protected Action action;

    public ActionPresenter(Action action) {
        this.action = action;
    }

    @Override
    protected void updateModel(String selectedValue) {
        action.execute();
    }

    @Override
    public void init() {
        // NOP
    }

    @Override
    protected abstract View routeToView(String selectedValue);
}
