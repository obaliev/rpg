package org.balev.rpg.cli.mvp.presenter.preinitialized.action;

import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.mvp.presenter.PresenterHolder;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.factory.ViewFactory;
import org.balev.rpg.engine.domain.action.Action;

/**
 * Created by obaliev on 9/21/15.
 */
public class ChangeLocationActionPresenter extends ActionPresenter {

    public ChangeLocationActionPresenter(Action action) {
        super(action);
    }

    @Override
    protected View routeToView(String selectedValue) {
        return ViewFactory.getInstance().createStaticView(PresenterHolder.executedActionPresenter,
                String.format("Our hero now in '%s' location\n(Press Enter to continue)",
                        GameMasterManager.getMainGameCharacter().getLocation().getName()));
    }
}
