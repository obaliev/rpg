package org.balev.rpg.cli.mvp.view;

import org.balev.rpg.cli.mvp.presenter.Presenter;

/**
 * Created by obaliev on 9/20/15.
 */
public abstract class View {
    protected String state;

    protected Presenter presenter;

    public View(Presenter presenter) {
        this.presenter = presenter;
        presenter.setView(this);
        initState();
    }

    public final String getState() {
        return state;
    }

    public final void updateState(String state) {
        presenter.updateModelAndView(state);
    }

    /**
     * Methos where view state must be defined
     */
    public abstract void initState();
}
