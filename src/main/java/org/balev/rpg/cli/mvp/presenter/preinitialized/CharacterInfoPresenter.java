package org.balev.rpg.cli.mvp.presenter.preinitialized;

import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicView;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;
import org.balev.rpg.engine.domain.GameCharacter;

/**
 * Created by obaliev on 9/20/15.
 */
public class CharacterInfoPresenter extends PreInitializedPresenter {

    @Override
    public void init() {
        GameCharacter gameCharacter = GameMasterManager.getMainGameCharacter();

        String state = view.getState();

        String characterInfo = String.format("Name: %s, Life: %d, Experience: %d, currentLocation: %s\n",
                gameCharacter.getName(), gameCharacter.getLife(),
                gameCharacter.getExperience(), gameCharacter.getLocation().getName());

        ((DynamicView) view).setState(characterInfo + state);
    }

    @Override
    protected void updateModel(String selectedValue) {
        // NOP
    }

    @Override
    protected View routeToView(String selectedValue) {
        return StaticViewHolder.gameMenuView;
    }
}
