package org.balev.rpg.cli.mvp.view.static_;

import org.balev.rpg.cli.mvp.presenter.Presenter;
import org.balev.rpg.cli.mvp.view.View;

/**
 * Common view with predefined state
 *
 * Created by obaliev on 9/20/15.
 */
public abstract class StaticView extends View {
    public StaticView(Presenter presenter) {
        super(presenter);
    }
}
