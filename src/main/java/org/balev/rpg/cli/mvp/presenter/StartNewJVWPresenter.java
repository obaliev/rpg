package org.balev.rpg.cli.mvp.presenter;

import org.balev.rpg.cli.GameMasterManager;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.static_.StaticViewHolder;
import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;

import java.util.Random;

/**
 * Created by obaliev on 9/20/15.
 */
public class StartNewJVWPresenter extends Presenter {

    private static final Integer DEFAULT_MAIN_CHARACTER_LIFE = 5;

    @Override
    protected boolean validate(String selectedValue) {
        return !selectedValue.isEmpty();
    }

    @Override
    protected void updateModel(String selectedValue) {
        GameCharacter mainGameCharacter = new GameCharacter(selectedValue, DEFAULT_MAIN_CHARACTER_LIFE);

        GameMasterManager.setMainGameCharacter(mainGameCharacter);

        // place created character into random location
        GameWorld gameWorld = GameMasterManager.getGameWorld();
        Location startLocation = gameWorld.getMapLocations().get(new Random().nextInt(gameWorld.getMapLocations().size()));
        startLocation.putGameCharacter(mainGameCharacter);
    }

    @Override
    protected View routeToView(String selectedValue) {
        return StaticViewHolder.startedNewGameMenuView;
    }
}