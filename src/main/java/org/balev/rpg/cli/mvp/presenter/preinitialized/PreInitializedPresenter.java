package org.balev.rpg.cli.mvp.presenter.preinitialized;

import org.balev.rpg.cli.mvp.presenter.Presenter;
import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicView;

/**
 * Created by obaliev on 9/20/15. <br />
 * <p>
 * Presenter which must do something before initializing view, for ex: <br />
 * When we need on view different text depends on state of the current game <br />
 * <br />
 * for one case it can be: <br />
 * 1. Do Something <br />
 * 2. Do Something2 <br />
 * <br />
 * for another case only: <br />
 * 1. Do something
 */
public abstract class PreInitializedPresenter extends Presenter {

    /**
     * Here should be the logic which we want to execute before show to the user view,
     * i.e. dynamically define view state
     */
    public abstract void init();

    @Override
    public void setView(View view) {
        if (!(view instanceof DynamicView)) {
            throw new IllegalArgumentException("Pre Initialized presenter must use only Dynamic View with manageable state");
        }

        super.setView(view);
    }
}
