package org.balev.rpg.cli.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Aleksandr Balev on 25.07.2015.
 */
public final class CLIUtils {

    private static BufferedReader reader;

    static {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    private CLIUtils() {
    }

    public static int readMenuInput(int menuHighValue) throws IOException {
        int menuChose = -1;

        while (menuChose < 1 || menuChose > menuHighValue) {
            try {
                menuChose = Integer.parseInt(reader.readLine());
            } catch (NumberFormatException ex) {
                menuChose = -1;
            }
        }

        return menuChose;
    }

    /**
     * Copy/Paste from lib jline jline.console.ConsoleReader#clearScreen()
     *
     * @see jline on github
     */
    public static void clr() throws IOException {
        System.out.write(27);
        System.out.write('[');
        System.out.write('2');
        System.out.write('J');
        System.out.flush();

        System.out.write(27);
        System.out.write('[');
        System.out.write('1');
        System.out.write(';');
        System.out.write('1');
        System.out.write('H');
        System.out.flush();
    }

    public static void println(String output) {
        System.out.println(output);
    }

    public static void print(String output) {
        System.out.print(output);
    }

    public static void drawLine() {
        println("-------------------------------------");
    }

    public static BufferedReader getReader() {
        return reader;
    }
}
