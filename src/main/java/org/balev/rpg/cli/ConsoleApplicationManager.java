package org.balev.rpg.cli;

import org.balev.rpg.cli.mvp.view.View;
import org.balev.rpg.cli.mvp.view.dynamic.DynamicView;

/**
 * Created by obaliev on 9/20/15.
 */
public class ConsoleApplicationManager {
    private static View activeView;

    public static View getActiveView() {
        return activeView;
    }

    public static void setActiveView(View activeView) {
        if (activeView instanceof DynamicView) {
            ((DynamicView) activeView).initializeView();
        }

        ConsoleApplicationManager.activeView = activeView;
    }
}
