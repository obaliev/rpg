package org.balev.rpg.domain;

import org.balev.rpg.engine.domain.GameCharacter;

/**
 * Created by obaliev on 8/2/15.
 */
public class GamePlayer {

    private GameCharacter gameCharacter;

    public GamePlayer() {
    }

    public GamePlayer(GameCharacter gameCharacter) {
        this.gameCharacter = gameCharacter;
    }

    public void createGameCharacter(String name, int life) {
        this.gameCharacter = new GameCharacter(name, life);
    }

    public GameCharacter getGameCharacter() {
        return gameCharacter;
    }
}
