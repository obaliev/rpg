package org.balev.rpg.domain;

import org.balev.rpg.engine.domain.GameCharacter;

/**
 * Created by obaliev on 8/2/15.
 */
public class GameStory {

    private GamePlayer mainGamePlayer;

    public void setMainGamePlayer(GamePlayer gamePlayer) {
        this.mainGamePlayer = gamePlayer;
    }

    public GameCharacter getMainGameCharacter() {
        return mainGamePlayer.getGameCharacter();
    }
}
