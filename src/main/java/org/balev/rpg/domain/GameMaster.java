package org.balev.rpg.domain;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.balev.rpg.engine.domain.Map;

/**
 * Created by obaliev on 8/2/15.
 */
public class GameMaster {

    private GameWorld.GameWorldBuilder gameWorldBuilder;
    private GameStory gameStory;
    private GameWorld gameWorld;

    public void startCreatingNewGameWorld(String gameWorldName) {
        this.gameWorldBuilder = new GameWorld.GameWorldBuilder(gameWorldName);
    }

    public void startCreatingNewGameWorld(String gameWorldName, Map map) {
        this.gameWorldBuilder = new GameWorld.GameWorldBuilder(gameWorldName, map);
    }

    public void bindLocations(Location location1, Location location2) {
        gameWorldBuilder.markLocationsAsNearbyToEachOther(location1, location2);
    }

    public void bindLocations(String locationName1, String locationName2) {
        Location location1 = gameWorldBuilder.findAddedLocationByName(locationName1);
        Location location2 = gameWorldBuilder.findAddedLocationByName(locationName2);

        bindLocations(location1, location2);
    }

    public void createStory() {
        this.gameStory = new GameStory();
    }

    public void addGamePlayer(GamePlayer gamePlayer) {
        gameStory.setMainGamePlayer(gamePlayer);
    }

    public void generateGameCharacters(int gameCharactersCount, String locationName) {
        Location location = gameWorldBuilder.findAddedLocationByName(locationName);
        for (int i = 0; i < gameCharactersCount; i++) {
            location.putGameCharacter(new GameCharacter("GameCharacter" + i, 3));
        }
    }

    public GameCharacter createGameCharacter(String gameCharacterName, int life) {
        return new GameCharacter(gameCharacterName, life);
    }

    public void putGameCharacterToLocation(GameCharacter gameCharacter, Location location) {
        location.putGameCharacter(gameCharacter);
    }

    public void putGameCharacterToLocation(GameCharacter gameCharacter, String locationName) {
        putGameCharacterToLocation(gameCharacter, gameWorldBuilder.findAddedLocationByName(locationName));
    }

    public GameWorld buildGameWorld() {
        gameWorld = gameWorldBuilder.build();
        return gameWorld;
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public GameCharacter getMainGameCharacter() {
        return gameStory.getMainGameCharacter();
    }

    public Location createLocation(String locationName) {
        Location location = new Location(locationName);
        gameWorldBuilder.addLocation(location);
        return location;
    }
}
