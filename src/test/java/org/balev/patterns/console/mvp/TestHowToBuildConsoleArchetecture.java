package org.balev.patterns.console.mvp;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by obaliev on 9/19/15.
 */
public class TestHowToBuildConsoleArchetecture {

    @Test
    public void testRouting() throws Exception {
        // GIVEN
        PresenterHolder.HelloPresenter = spy(PresenterHolder.HelloPresenter);

        HighLevelManager.setActiveView(ViewHolder.HelloView);
        assertThat(HighLevelManager.getActiveView().getState(), is("Just Say Hello To The System..."));

        // WHEN
        HighLevelManager.getActiveView().updateState("some state");

        // THEN
        verify(PresenterHolder.HelloPresenter).updateModelAndView(anyString());
        assertThat(HighLevelManager.getActiveView(), is(ViewHolder.ByeView));
        assertThat(HighLevelManager.getActiveView().getState(), is("Say good bye..."));
    }
}

class HighLevelManager {
    private static View activeView;

    public static View getActiveView() {
        return activeView;
    }

    public static void setActiveView(View activeView) {
        HighLevelManager.activeView = activeView;
    }
}

abstract class View {
    protected String state;

    private Presenter presenter;

    public View(Presenter presenter) {
        this.presenter = presenter;
        presenter.setView(this);
        initState();
    }

    public String getState() {
        return state;
    }

    public void updateState(String state) {
        this.state = state;
        presenter.updateModelAndView(state);
    }

    public abstract void initState();
}

class ViewHolder {
    static View HelloView = new View(PresenterHolder.HelloPresenter) {
        @Override
        public void initState() {
            state = "Just Say Hello To The System...";
        }
    };
    static View ByeView = new View(PresenterHolder.ByePresenter) {
        @Override
        public void initState() {
            state = "Say good bye...";
        }
    };
}

abstract class Presenter {
    private View view;

    public void setView(View view) {
        this.view = view;
    }

    public void updateModelAndView(String selectedValue) {
        validate(selectedValue);
        updateModel();
        HighLevelManager.setActiveView(routeToView(selectedValue));
    }

    protected void validate(String selectedValue) {
        // NOP
    }

    protected abstract void updateModel();

    protected abstract View routeToView(String selectedValue);
}

class HelloPresenter extends Presenter {
    @Override
    public void updateModel() {
        System.out.println("Hello, wie geht's?");
    }

    @Override
    protected View routeToView(String selectedValue) {
        return ViewHolder.ByeView;
    }
}

class ByePresenter extends Presenter {
    @Override
    public void updateModel() {
        System.out.println("Bye!");
    }

    @Override
    protected View routeToView(String selectedValue) {
        return ViewHolder.HelloView;
    }
}

class PresenterHolder {
    static Presenter ByePresenter = new ByePresenter();
    static Presenter HelloPresenter = new HelloPresenter();
}