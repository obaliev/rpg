package org.balev.patterns.mvp;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Created by obaliev on 9/17/15.
 */
public class TestWhatIsMVP {

    @Test
    public void testWhatIsMVP() throws Exception {

        Model model = new Model(1, "Simple");

        Presenter presenter = new Presenter(model);
        View view = new View(presenter);

        System.out.println(view.getState());
        view.change();
        System.out.println(view.getState());

    }
}

class View {
    private String state;

    private Presenter presenter;

    public View(Presenter presenter) {
        this.presenter = presenter;
        presenter.setView(this);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void change() {
        presenter.changeModel();
    }
}

class Presenter {

    private Model model;
    private View view;

    public Presenter(Model model) {
        this.model = model;
    }

    public void setView(View view) {
        this.view = view;
    }

    public void changeModel() {
        model.change();
        view.setState(model.toString());
    }
}

class Model {
    private Integer id;
    private String type;
    private LocalDateTime lastChangeDate;

    public Model(Integer id, String type) {
        this.id = id;
        this.type = type;
        this.lastChangeDate = LocalDateTime.now();
    }

    public String change() {
        lastChangeDate = LocalDateTime.now();

        return lastChangeDate.toString();
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", lastChangeDate=" + lastChangeDate +
                '}';
    }
}