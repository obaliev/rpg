package org.balev.patterns.mvc;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Created by obaliev on 9/17/15.
 */
public class TestWhatIsMVC {

    @Test
    public void testWhatIsMVC() throws Exception {

        Model model = new Model(1, "Simple");

        Controller controller1 = new Controller(model);
        Controller controller2 = new Controller(model);

        System.out.println(controller1.changeModel().getState());
        System.out.println(controller2.changeModel().getState());
    }
}

class Controller {
    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public View changeModel() {
        model.change();

        return generateView();
    }

    private View generateView() {
        return new View(model.toString());
    }
}

class View {
    private final String state;

    public View(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}

class Model {
    private Integer id;
    private String type;
    private LocalDateTime lastChangeDate;

    public Model(Integer id, String type) {
        this.id = id;
        this.type = type;
        this.lastChangeDate = LocalDateTime.now();
    }

    public void change() {
        lastChangeDate = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", lastChangeDate=" + lastChangeDate +
                '}';
    }
}