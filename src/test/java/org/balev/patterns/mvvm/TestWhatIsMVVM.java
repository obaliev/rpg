package org.balev.patterns.mvvm;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Created by obaliev on 9/18/15.
 */
public class TestWhatIsMVVM {

    @Test
    public void testWhatIsMVVM() throws Exception {

        Model model = new Model(1, "Simple");
        View view = new View();
        ViewModel viewModel = new ViewModel(view, model);

        /**
         * Model changed intirely
         */
        Thread thread = new Thread(() -> model.change());
        thread.start();
        thread.join();
        System.out.println(view.getState());

        /**
         * Changes are from user
         */
        view.changeStateByClient("2015-09-17T23:52:15.442");
        System.out.println(view.getState());
    }
}

class View {

    private String state;
    private ViewModel viewModel;

    public void setViewModel(ViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void changeStateByClient(String newState) {
        this.state = newState;
        viewModel.changeState(newState);
    }

}

class ViewModel {

    private View view;
    private Model model;

    public ViewModel(View view, Model model) {
        this.view = view;
        this.model = model;

        view.setViewModel(this);
        model.setViewModel(this);
    }

    public void propagateChangesToView() {
        view.setState(model.toString());
    }

    public void changeState(String state) {
        model.setLastChangeDate(state);
    }
}

class Model {
    private Integer id;
    private String type;
    private LocalDateTime lastChangeDate;

    private ViewModel viewModel;

    public Model(Integer id, String type) {
        this.id = id;
        this.type = type;
        this.lastChangeDate = LocalDateTime.now();
    }

    public void setViewModel(ViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void change() {
        lastChangeDate = LocalDateTime.now();
        viewModel.propagateChangesToView();
    }

    public void setLastChangeDate(String lastChangeDate) {
        this.lastChangeDate = LocalDateTime.parse(lastChangeDate);
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", lastChangeDate=" + lastChangeDate +
                '}';
    }
}


