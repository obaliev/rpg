package org.balev.patterns.fowler.mvc.smalltalk;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Created by obaliev on 9/17/15.
 */
public class TestWhatIsSmalltalksMVC {

    @Test
    public void TestWhatIsSmalltalksMVC() throws Exception {

        Model model = new Model(1, "Simple");

        Controller controller = new Controller(model);

        View view = new View(model);

        System.out.println(view.getState());

        controller.changeModel();
        System.out.println(view.getState());

        view.propagateStateChanges("2015-09-17T23:52:15.442");
        System.out.println(view.getState());
    }
}

class Controller {
    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public void changeModel() {
        model.change();
    }
}

class View {

    private Model model;

    private String state;

    public View(Model model) {
        this.model = model;
        this.state = this.model.toString();

        model.setView(this);
    }

    public void propagateStateChanges(String newState) {
        this.state = newState;
        model.setLastChangeDate(state);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

class Model {

    private View view;

    private Integer id;
    private String type;
    private LocalDateTime lastChangeDate;

    public Model(Integer id, String type) {
        this.id = id;
        this.type = type;
        this.lastChangeDate = LocalDateTime.now();
    }

    public void setLastChangeDate(String lastChangeDate) {
        this.lastChangeDate = LocalDateTime.parse(lastChangeDate);
    }

    public void change() {
        lastChangeDate = LocalDateTime.now();
        view.setState(this.toString());
    }

    public void setView(View view) {
        this.view = view;
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", lastChangeDate=" + lastChangeDate +
                '}';
    }
}