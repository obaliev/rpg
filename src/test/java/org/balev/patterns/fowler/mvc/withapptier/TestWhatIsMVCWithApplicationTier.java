package org.balev.patterns.fowler.mvc.withapptier;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Created by obaliev on 9/17/15.
 */
public class TestWhatIsMVCWithApplicationTier {

    @Test
    public void testWhatIsSmalltalksMVC() throws Exception {

        ApplicationModel applicationModel = new ApplicationModel();

        Model model = new Model(1, "Simple");
        model.setApplicationModel(applicationModel);

        View view = new View(applicationModel);

        Controller controller = new Controller(model);

        System.out.println(view.getState());

        controller.changeModel();
        System.out.println(view.getState());

        view.propagateStateChanges("2015-09-17T23:52:15.442");
        System.out.println(view.getState());
    }
}

class Controller {
    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public void changeModel() {
        model.change();
    }
}

class ApplicationModel {

    private View view;
    private Model model;

    public ApplicationModel() {
    }

    public void setView(View view) {
        this.view = view;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getModelState() {
        return model.toString();
    }

    public void setModelsLastChangeDate(String state) {
        model.setLastChangeDate(state);
    }

    public void setViewsState(String state) {
        view.setState(state);
    }
}

class View {

    private ApplicationModel applicationModel;

    private String state;

    public View(ApplicationModel applicationModel) {
        this.applicationModel = applicationModel;
        this.state = applicationModel.getModelState();

        applicationModel.setView(this);
    }

    public void propagateStateChanges(String newState) {
        this.state = newState;
        applicationModel.setModelsLastChangeDate(state);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

class Model {

    private ApplicationModel applicationModel;

    private Integer id;
    private String type;
    private LocalDateTime lastChangeDate;

    public Model(Integer id, String type) {
        this.id = id;
        this.type = type;
        this.lastChangeDate = LocalDateTime.now();
    }

    public void setLastChangeDate(String lastChangeDate) {
        this.lastChangeDate = LocalDateTime.parse(lastChangeDate);
    }

    public void change() {
        lastChangeDate = LocalDateTime.now();
        applicationModel.setViewsState(this.toString());
    }

    public void setApplicationModel(ApplicationModel applicationModel) {
        this.applicationModel = applicationModel;
        applicationModel.setModel(this);
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", lastChangeDate=" + lastChangeDate +
                '}';
    }
}