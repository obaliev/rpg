package org.balev.patterns.vm;

import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Created by obaliev on 9/18/15.
 */
public class TestWhatIsVM {

    @Test
    public void testWhatIsVM() throws Exception {

        Model model = new Model(1, "Simple");
        View view = new View(model);

        /**
         * Initial state
         */
        System.out.println(view.getState());

        /**
         * Changes on client side
         */
        view.setState("2015-09-17T23:52:15.442");
        System.out.println(view.getState());

        /**
         * Changes on model side
         */
        model.change();
        System.out.println(view.getState());
    }
}

class View {

    private Model model;
    private String state;

    public View(Model model) {
        this.model = model;
        model.setView(this);
        setState(model.toString());
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void changeState(String state) {
        model.setLastChangeDate(state);
    }
}

class Model {
    private Integer id;
    private String type;
    private LocalDateTime lastChangeDate;

    private View view;

    public Model(Integer id, String type) {
        this.id = id;
        this.type = type;
        this.lastChangeDate = LocalDateTime.now();
    }

    public void change() {
        lastChangeDate = LocalDateTime.now();
        view.setState(this.toString());
    }

    public void setView(View view) {
        this.view = view;
    }

    public void setLastChangeDate(String lastChangeDate) {
        this.lastChangeDate = LocalDateTime.parse(lastChangeDate);
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", lastChangeDate=" + lastChangeDate +
                '}';
    }
}