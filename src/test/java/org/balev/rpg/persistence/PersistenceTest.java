package org.balev.rpg.persistence;

import org.apache.commons.io.FileUtils;
import org.balev.rpg.persistance.PersistenceService;
import org.junit.Before;
import org.junit.BeforeClass;

import java.io.File;

/**
 * Created by Aleksandr Balev on 25.07.2015.
 */
public abstract class PersistenceTest {

    protected static final File PERSISTENCE_DIR = new File(FileUtils.getTempDirectoryPath() + "/save");
    protected PersistenceService persistenceService = new PersistenceService(PERSISTENCE_DIR.getPath());

    @BeforeClass
    public static void init() throws Exception {
        if (!PERSISTENCE_DIR.exists()) {
            FileUtils.forceMkdir(PERSISTENCE_DIR);
        }
    }

    @Before
    public void setUp() throws Exception {
        FileUtils.cleanDirectory(PERSISTENCE_DIR);
    }
}
