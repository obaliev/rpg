package org.balev.rpg.persistence;

import org.balev.rpg.cli.domain.RPG;
import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.balev.rpg.persistance.PersistenceService;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created by Aleksandr Balev on 23.07.2015.
 */
public class TestPersistenceService extends PersistenceTest {

    private static final GameWorld DEFAULT_GAME_WORLD = new GameWorld.GameWorldBuilder("Game World").addLocation(new Location("location")).build();
    private static final RPG DEFAULT_RPG = new RPG(DEFAULT_GAME_WORLD);
    private static final GameCharacter DEFAULT_MAIN_GAME_CHARACTER = new GameCharacter("Main GC Name", 1);

    static {
        DEFAULT_GAME_WORLD.getMapLocations().get(0).putGameCharacter(DEFAULT_MAIN_GAME_CHARACTER);
        DEFAULT_RPG.setMainGameCharacter(DEFAULT_MAIN_GAME_CHARACTER);
    }

    @Test
    public void testSaveGameWorldCreatesANewDirectoryToSaveSerializationObjects() throws Exception {
        // WHEN
        assertTrue(PERSISTENCE_DIR.exists());
        assertThat(PERSISTENCE_DIR.listFiles().length, is(0));
        persistenceService.saveGameWorld(DEFAULT_GAME_WORLD);

        // THEN
        assertThat(PERSISTENCE_DIR.listFiles().length, is(1));
        assertThat(PERSISTENCE_DIR.listFiles()[0].getName(), containsString(PersistenceService.SAVE_FILE_PREFIX));
    }

    @Test
    public void testFindSavedGameWorlds() throws Exception {
        // GIVEN
        long timeBeforeSave = new Date().getTime();
        persistenceService.saveGameWorld(DEFAULT_GAME_WORLD);

        // WHEN
        assertThat(PERSISTENCE_DIR.listFiles().length, is(1));
        assertThat(PERSISTENCE_DIR.listFiles()[0].getName(), containsString(PersistenceService.SAVE_FILE_PREFIX));
        List<Date> savedGameWorlds = persistenceService.findSavedFileDates();

        // THEN
        assertThat(savedGameWorlds.size(), is(1));
        assertThat(savedGameWorlds.get(0).getTime(), greaterThanOrEqualTo(timeBeforeSave));
    }

    @Test
    public void testResumeSavedGameWorld() throws Exception {
        // GIVEN
        persistenceService.saveGameWorld(DEFAULT_GAME_WORLD);
        List<Date> savedGameWorlds = persistenceService.findSavedFileDates();
        assertThat(PERSISTENCE_DIR.listFiles().length, is(1));

        // WHEN
        GameWorld gameWorld = persistenceService.resumeSavedGame(savedGameWorlds.get(0));

        // THEN
        assertNotNull(gameWorld);
        assertEquals(DEFAULT_GAME_WORLD, gameWorld);
    }

    @Test
    public void testSaveRPG() {
        // WHEN SAVE
        assertTrue(PERSISTENCE_DIR.exists());
        assertThat(PERSISTENCE_DIR.listFiles().length, is(0));
        persistenceService.saveRPG(DEFAULT_RPG);

        // THEN
        assertThat(PERSISTENCE_DIR.listFiles().length, is(1));
        assertThat(PERSISTENCE_DIR.listFiles()[0].getName(), containsString(PersistenceService.SAVE_FILE_PREFIX));
    }

    @Test
    public void testFindSavedRPGs() throws Exception {
        // GIVEN
        long timeBeforeSave = new Date().getTime();
        persistenceService.saveRPG(DEFAULT_RPG);

        // WHEN
        assertThat(PERSISTENCE_DIR.listFiles().length, is(1));
        assertThat(PERSISTENCE_DIR.listFiles()[0].getName(), containsString(PersistenceService.SAVE_FILE_PREFIX));
        List<Date> savedGameWorlds = persistenceService.findSavedFileDates();

        // THEN
        assertThat(savedGameWorlds.size(), is(1));
        assertThat(savedGameWorlds.get(0).getTime(), greaterThanOrEqualTo(timeBeforeSave));
    }

    @Test
    public void testResumeSavedRPG() throws Exception {
        // GIVEN
        persistenceService.saveRPG(DEFAULT_RPG);
        List<Date> savedRPGs = persistenceService.findSavedFileDates();
        assertThat(PERSISTENCE_DIR.listFiles().length, is(1));

        // WHEN
        RPG rpg = persistenceService.resumeRPG(savedRPGs.get(0));

        // THEN
        assertNotNull(rpg);
        assertEquals(DEFAULT_RPG, rpg);
    }
}
