package org.balev.rpg.persistence;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Aleksandr Balev on 25.07.2015.
 */
public class PerformanceTestSaveAndResume extends PersistenceTest {

    public static int getNextRandomInt(int upperNotIncludedValue) {
        return new Random().nextInt(upperNotIncludedValue);
    }

    @Test
    public void performanceTestSaveAndResumeGameWithBigAmountOfObjects() throws Exception {
        // GIVEN
        final int gameCharactersCount = 1000;
        final int locationsCount = 25;

        GameWorld.GameWorldBuilder gameWorldBuilder = new GameWorld.GameWorldBuilder("Game World");

        List<GameCharacter> gameCharacters = new ArrayList<>(gameCharactersCount);
        for (int i = 0; i < gameCharactersCount; i++) {
            gameCharacters.add(new GameCharacter("Game Character #" + i, 1));
        }

        List<Location> locations = new ArrayList<Location>(locationsCount);
        for (int i = 0; i < locationsCount; i++) {
            Location location = new Location("Location #" + i);
            locations.add(location);
            gameWorldBuilder.addLocation(location);
        }

        // Randomly set nearby locations()
        for (int i = 0; i < locationsCount; i++) {
            Location sourceLocation = locations.get(getNextRandomInt(locationsCount));
            Location targetLocation = locations.get(getNextRandomInt(locationsCount));

            // in case of collisions
            if (sourceLocation.equals(targetLocation) || sourceLocation.containsNearbyLocation(targetLocation)) {
                continue;
            }

            gameWorldBuilder.markLocationsAsNearbyToEachOther(sourceLocation, targetLocation);
        }

        // Randomly place game characters
        for (int i = 0; i < gameCharactersCount; i++) {
            int randomLocationToPlace = getNextRandomInt(locationsCount);
            locations.get(randomLocationToPlace).putGameCharacter(gameCharacters.get(i));
        }

        GameWorld gameWorld = gameWorldBuilder.build();

        persistenceService.saveGameWorld(gameWorld);
        Date savedDate = persistenceService.findSavedFileDates().get(0);

        // WHEN
        GameWorld resumedGameWorld = persistenceService.resumeSavedGame(savedDate);

        // THEN
        assertNotNull(resumedGameWorld);
        assertEquals(gameWorld, resumedGameWorld);
    }
}
