package org.balev.rpg.persistence;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class TestSaveAndResume extends PersistenceTest {

    @Test
    public void testSaveGame() throws Exception {
        // GIVEN
        GameCharacter mainGameCharacter = new GameCharacter("Main Character Name", 5);
        GameCharacter secondGameCharacter = new GameCharacter("Second Character Name", 1);

        Location location = new Location("location");
        Location secondLocation = new Location("location");

        location.putGameCharacter(mainGameCharacter);
        location.putGameCharacter(secondGameCharacter);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World").addLocation(location).addNewLocationAndMarkItNearbyToExistingLocation(secondLocation, location).build();

        // WHEN
        persistenceService.saveGameWorld(gameWorld);

        // THEN
    }

    @Test
    public void testResumeGame() throws Exception {
        // GIVEN
        GameCharacter firstLocationFirstGameCharacter = new GameCharacter("firstLocationFirstGameCharacter Name", 5);
        GameCharacter firstLocationSecondGameCharacter = new GameCharacter("firstLocationSecondGameCharacter Name", 1);
        GameCharacter secondLocationGameCharacter = new GameCharacter("secondLocationGameCharacter Name", 1);
        GameCharacter thirdLocationGameCharacter = new GameCharacter("thirdLocationGameCharacter Name", 1);

        Location firstLocation = new Location("first location");
        Location secondLocation = new Location("second Location");
        Location thirdLocation = new Location("third Location");
        Location fourthLocation = new Location("fourth Location");

        firstLocation.putGameCharacter(firstLocationFirstGameCharacter);
        firstLocation.putGameCharacter(firstLocationSecondGameCharacter);
        secondLocation.putGameCharacter(secondLocationGameCharacter);
        thirdLocation.putGameCharacter(thirdLocationGameCharacter);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World")
                .addLocation(firstLocation)
                .addNewLocationAndMarkItNearbyToExistingLocation(secondLocation, firstLocation)
                .addNewLocationAndMarkItNearbyToExistingLocation(thirdLocation, secondLocation)
                .addNewLocationAndMarkItNearbyToExistingLocation(fourthLocation, firstLocation)
                .build();

        persistenceService.saveGameWorld(gameWorld);
        Date savedDate = persistenceService.findSavedFileDates().get(0);

        // WHEN
        GameWorld resumedGameWorld = persistenceService.resumeSavedGame(savedDate);

        // THEN
        assertNotNull(resumedGameWorld);
        assertEquals(gameWorld, resumedGameWorld);
    }
}
