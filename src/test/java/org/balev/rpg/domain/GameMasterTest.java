package org.balev.rpg.domain;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Created by obaliev on 10/3/15.
 */
public class GameMasterTest {

    @Test
    public void testBuildGameWorld() throws Exception {
        GamePlayer gamePlayer = new GamePlayer();
        gamePlayer.createGameCharacter("Name", 40);

        GameMaster gameMaster = new GameMaster();

        gameMaster.startCreatingNewGameWorld("My simple World");

        gameMaster.createLocation("new location1");
        gameMaster.createLocation("new location2");
        gameMaster.bindLocations("new location1", "new location2");

        gameMaster.createStory();
        gameMaster.addGamePlayer(gamePlayer);

        GameCharacter gameCharacter = gameMaster.createGameCharacter("Character Name", 5);
        gameMaster.putGameCharacterToLocation(gameCharacter, "new location1");
        gameMaster.generateGameCharacters(2, "new location2");

        GameWorld gameWorld = gameMaster.buildGameWorld();
        GameCharacter mainGameCharacter = gameMaster.getMainGameCharacter();

        assertNotNull(gameWorld);
        assertNotNull(mainGameCharacter);

        assertThat(gameWorld.getMapLocations().size(), is(2));
        assertThat(gameWorld.getName(), is("My simple World"));

        assertThat(mainGameCharacter.getName(), is("Name"));
        assertThat(mainGameCharacter.getLife(), is(40));
    }
}