package org.balev.rpg.engine;

import org.balev.rpg.engine.domain.GameCharacter;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class TestCreateCharacter {

    @Test
    public void testCreateGenericGameCharacter() throws Exception {
        final String testGCName = "Test GC Name";
        final Integer testGCLife = 5;

        GameCharacter mainThreadCharacter = new GameCharacter(testGCName, testGCLife);

        assertThat(mainThreadCharacter.getName(), is(testGCName));
        assertThat(mainThreadCharacter.getLife(), is(testGCLife));
        assertThat(mainThreadCharacter.getExperience(), is(0));
        assertNull(mainThreadCharacter.getLocation());
    }
}
