package org.balev.rpg.engine.util;

import org.balev.rpg.engine.domain.action.Action;
import org.balev.rpg.engine.domain.action.ChangeLocationAction;
import org.balev.rpg.engine.domain.action.FightAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr Balev on 22.07.2015.
 */
public final class ActionTestUtils {

    private ActionTestUtils() {
    }

    public static List<ChangeLocationAction> findChangeLocationActions(List<Action> availableActions) {
        List<ChangeLocationAction> changeLocationActions = new ArrayList<ChangeLocationAction>();

        for (Action action : availableActions) {
            if (ActionUtils.isChangeLocationAction(action)) {
                changeLocationActions.add((ChangeLocationAction) action);
            }
        }

        return changeLocationActions;
    }


    public static List<FightAction> findFightActions(List<Action> availableActions) {
        List<FightAction> fightActions = new ArrayList<FightAction>();

        for (Action action : availableActions) {
            if (ActionUtils.isFightAction(action)) {
                fightActions.add((FightAction) action);
            }
        }

        return fightActions;
    }
}
