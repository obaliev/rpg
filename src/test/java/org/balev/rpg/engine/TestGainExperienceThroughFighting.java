package org.balev.rpg.engine;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.balev.rpg.engine.domain.action.FightAction;
import org.balev.rpg.engine.util.ActionTestUtils;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class TestGainExperienceThroughFighting {

    @Test
    public void testGainExperienceThroughFightingWithWeakGuy() throws Exception {
        // GIVEN
        GameCharacter mainGameCharacter = new GameCharacter("Main Character Name", 5);
        GameCharacter gameCharacterToKill = new GameCharacter("Weak Character Name", 1);

        Location location = new Location("location");

        location.putGameCharacter(mainGameCharacter);
        location.putGameCharacter(gameCharacterToKill);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World").addLocation(location).build();

        List<FightAction> fightActions = ActionTestUtils.findFightActions(mainGameCharacter.explore());
        assertThat(fightActions.size(), greaterThanOrEqualTo(1));

        // WHEN
        assertThat(mainGameCharacter.getExperience(), is(0));
        fightActions.get(0).execute();

        // THEN
        assertTrue(mainGameCharacter.isAlive());
        assertFalse(gameCharacterToKill.isAlive());

        assertThat(mainGameCharacter.getExperience(), is(1));
    }

    @Test
    public void testGainExperienceThroughFightingWithExperiencedGuy() throws Exception {
        // GIVEN
        GameCharacter mainGameCharacter = new GameCharacter("Main Character Name", 5) {
        };
        GameCharacter gameCharacterToKill = new GameCharacter("Experienced Character Name", 1) {
        };

        final Integer experiencedGCExp = 3;
        gameCharacterToKill.gainExperience(experiencedGCExp);

        Location location = new Location("location");

        location.putGameCharacter(mainGameCharacter);
        location.putGameCharacter(gameCharacterToKill);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World").addLocation(location).build();

        List<FightAction> fightActions = ActionTestUtils.findFightActions(mainGameCharacter.explore());
        assertThat(fightActions.size(), greaterThanOrEqualTo(1));

        // WHEN
        assertThat(mainGameCharacter.getExperience(), is(0));
        fightActions.get(0).execute();

        // THEN
        assertTrue(mainGameCharacter.isAlive());
        assertFalse(gameCharacterToKill.isAlive());

        assertThat(mainGameCharacter.getExperience(), is(experiencedGCExp));
    }

    @Test
    public void testFightingWithHeavyGuy() throws Exception {
        // GIVEN
        GameCharacter mainGameCharacter = new GameCharacter("Main Character Name", 5) {
        };
        GameCharacter heavyGameCharacter = new GameCharacter("Heavy Character Name", 10) {
        };

        Location location = new Location("location");

        location.putGameCharacter(mainGameCharacter);
        location.putGameCharacter(heavyGameCharacter);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World").addLocation(location).build();

        List<FightAction> fightActions = ActionTestUtils.findFightActions(mainGameCharacter.explore());
        assertThat(fightActions.size(), greaterThanOrEqualTo(1));

        // WHEN
        assertThat(mainGameCharacter.getExperience(), is(0));
        fightActions.get(0).execute();

        // THEN
        assertFalse(mainGameCharacter.isAlive());
        assertTrue(heavyGameCharacter.isAlive());

        assertThat(heavyGameCharacter.getExperience(), is(1));
    }

    @Test
    public void testFightingWithEqualStrength() throws Exception {
        // GIVEN
        GameCharacter mainGameCharacter = new GameCharacter("Main Character Name", 5) {
        };
        GameCharacter heavyGameCharacter = new GameCharacter("Heavy Character Name", 5) {
        };

        Location location = new Location("location");

        location.putGameCharacter(mainGameCharacter);
        location.putGameCharacter(heavyGameCharacter);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World").addLocation(location).build();

        List<FightAction> fightActions = ActionTestUtils.findFightActions(mainGameCharacter.explore());
        assertThat(fightActions.size(), greaterThanOrEqualTo(1));

        // WHEN
        assertThat(mainGameCharacter.getExperience(), is(0));
        fightActions.get(0).execute();

        // THEN
        assertTrue((mainGameCharacter.isAlive() && !heavyGameCharacter.isAlive())
                || (!mainGameCharacter.isAlive() && heavyGameCharacter.isAlive()));
    }
}


