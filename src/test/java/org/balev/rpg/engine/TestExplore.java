package org.balev.rpg.engine;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.balev.rpg.engine.domain.action.Action;
import org.balev.rpg.engine.domain.action.ChangeLocationAction;
import org.balev.rpg.engine.domain.action.FightAction;
import org.balev.rpg.engine.util.ActionTestUtils;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Created by Aleksandr Balev on 21.07.2015.
 */
public class TestExplore {

    @Test
    public void testExploreAllowAtLeastChangeLocationWhenAtLeastTwoLocationsExist() throws Exception {
        // GIVEN
        GameCharacter gameCharacter = new GameCharacter("Character Name", 1);
        Location sourceLocation = new Location("Source location");
        Location targetLocation = new Location("Target location");

        // TODO: Disable such approach
//        sourceLocation.addToNearbyLocations(targetLocation);

        sourceLocation.putGameCharacter(gameCharacter);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World").addLocation(sourceLocation).addNewLocationAndMarkItNearbyToExistingLocation(targetLocation, sourceLocation).build();

        // WHEN
        assertThat(gameWorld.getMapLocations().size(), greaterThanOrEqualTo(2));
        List<Action> possibleActions = gameCharacter.explore();

        // THEN
        assertThat(possibleActions.size(), greaterThanOrEqualTo(1));

        List<ChangeLocationAction> changeLocationActions = ActionTestUtils.findChangeLocationActions(possibleActions);
        assertThat(changeLocationActions.size(), greaterThanOrEqualTo(1));

        assertThat(gameCharacter.getLocation(), is(sourceLocation));
        // WHEN
        changeLocationActions.get(0).execute();

        //THEN
        assertThat(gameCharacter.getLocation(), is(targetLocation));

        // Test change location after change location
        // WHEN
        possibleActions = gameCharacter.explore();

        // THEN
        assertThat(possibleActions.size(), greaterThanOrEqualTo(1));

        changeLocationActions = ActionTestUtils.findChangeLocationActions(possibleActions);
        assertThat(changeLocationActions.size(), greaterThanOrEqualTo(1));

        assertThat(gameCharacter.getLocation(), is(targetLocation));
        // WHEN
        changeLocationActions.get(0).execute();

        //THEN
        assertThat(gameCharacter.getLocation(), is(sourceLocation));
    }

    @Test
    public void testExploreAllowFightingActionWhenTwoCharactersOnSameLocationExist() throws Exception {
        // GIVEN
        GameCharacter mainGameCharacter = new GameCharacter("Main Character Name", 5) {
        };
        GameCharacter gameCharacterToKill = new GameCharacter("Weak Character Name", 1) {
        };

        Location location = new Location("location");

        location.putGameCharacter(mainGameCharacter);
        location.putGameCharacter(gameCharacterToKill);
        GameWorld gameWorld = new GameWorld.GameWorldBuilder("New Game World").addLocation(location).build();

        // WHEN
        assertThat(gameWorld.getMapLocations().size(), is(1));
        assertThat(gameWorld.getMapLocations().get(0).getGameCharacters().size(), greaterThanOrEqualTo(2));
        List<Action> availableActions = mainGameCharacter.explore();

        // THEN
        assertThat(availableActions.size(), greaterThanOrEqualTo(1));

        List<FightAction> fightActions = ActionTestUtils.findFightActions(availableActions);
        assertThat(fightActions.size(), greaterThanOrEqualTo(1));

        // WHEN
        fightActions.get(0).execute();

        // THEN
        assertTrue(mainGameCharacter.isAlive());
        assertFalse(gameCharacterToKill.isAlive());
    }
}
