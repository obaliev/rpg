package org.balev.rpg.emulate;

import org.balev.rpg.engine.domain.GameCharacter;
import org.balev.rpg.engine.domain.GameWorld;
import org.balev.rpg.engine.domain.Location;
import org.balev.rpg.engine.domain.action.Action;
import org.balev.rpg.engine.domain.action.FightAction;
import org.balev.rpg.persistence.PersistenceTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Aleksandr Balev on 25.07.2015.
 *
 * Tests with no asserts, it's an end-to-end test which check fatal errors (exceptions) during the game
 */
public class TestEmulateWorld extends PersistenceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestEmulateWorld.class);

    public static int getNextRandomInt(int upperNotIncludedValue) {
        return new Random().nextInt(upperNotIncludedValue);
    }

    private static String getOverallGameCharacterInfo(GameCharacter gameCharacter) {
        return String.format("Name: '%s', Life: %d, Experience: %d, Location: '%s'",
                gameCharacter.getName(), gameCharacter.getLife(), gameCharacter.getExperience(), gameCharacter.getLocation().getName());
    }

    /**
     * Simple and funny test which allow generate simple world
     * Just adjust gameCharactersCount and locationsCount and look into logger output :)
     */
    @Test
    public void testEmulateWorldNotThrowingAnyExceptions() throws Exception {
        // GIVEN
        final int gameCharactersCount = 10;
        final int locationsCount = 4;

        GameWorld.GameWorldBuilder gameWorldBuilder = new GameWorld.GameWorldBuilder("Game World");

        List<GameCharacter> gameCharacters = new ArrayList<>(gameCharactersCount);
        for (int i = 0; i < gameCharactersCount; i++) {
            gameCharacters.add(new GameCharacter("Game Character #" + i, 1));
        }

        List<Location> locations = new ArrayList<>(locationsCount);
        for (int i = 0; i < locationsCount; i++) {
            Location location = new Location("Location #" + i);
            locations.add(location);
            gameWorldBuilder.addLocation(location);
        }

        // Randomly set nearby locations() - bad decision as in this case generates a lot of islands without nearby locations
        // Better make as a chain A <-> B <-> C ...
        for (int i = 1; i < locationsCount; i++) {
            Location sourceLocation = locations.get(i);
            Location targetLocation = locations.get(i - 1);

            gameWorldBuilder.markLocationsAsNearbyToEachOther(sourceLocation, targetLocation);
        }

        // Randomly place game characters
        for (int i = 0; i < gameCharactersCount; i++) {
            int randomLocationToPlace = getNextRandomInt(locationsCount);
            locations.get(randomLocationToPlace).putGameCharacter(gameCharacters.get(i));
        }

        GameWorld gameWorld = gameWorldBuilder.build();

        // START EMULATING :D
        final int emulateIterations = 5;
        LOGGER.info("Start emulating at " + new Date());
        for (int iteration = 0; iteration < emulateIterations; iteration++) {
            LOGGER.info("Iteration #" + iteration);
            for (GameCharacter gameCharacter : gameCharacters) {
                // add random to skip sometimes explore step
                if (gameCharacter.isAlive() && getNextRandomInt(3) == 1) {
                    List<Action> availableActions = gameCharacter.explore();

                    if (!availableActions.isEmpty()) {
                        // Make random action
                        Action action = availableActions.get(getNextRandomInt(availableActions.size()));
                        LOGGER.info(String.format("Before '%s' action: %s", action.getClass().getSimpleName(), getOverallGameCharacterInfo(gameCharacter)));
                        action.execute();
//                        LOGGER.info(getOverallGameCharacterInfo(gameCharacter));
                        if (action.getClass().equals(FightAction.class)) {
                            LOGGER.info(getOverallGameCharacterInfo(gameCharacter));
                            LOGGER.info(getOverallGameCharacterInfo(((FightAction) action).getSecondGameCharacter()));
                        }

                    }
                }
            }
        }
        LOGGER.info("Finish emulating at " + new Date());
        String overallCharactersReport = "";
        for (GameCharacter gameCharacter : gameCharacters) {
            overallCharactersReport += getOverallGameCharacterInfo(gameCharacter) + "\n";
        }
        LOGGER.info(String.format("\nOverall characters report:\n%s", overallCharactersReport));
    }

    @Test
    public void testEmulateWorldNotThrowingAnyExceptionsWhenStartSaveAndResume() throws Exception {
        // GIVEN
        final int gameCharactersCount = 10;
        final int locationsCount = 4;

        GameWorld.GameWorldBuilder gameWorldBuilder = new GameWorld.GameWorldBuilder("Game World");

        List<GameCharacter> gameCharacters = new ArrayList<>(gameCharactersCount);
        for (int i = 0; i < gameCharactersCount; i++) {
            gameCharacters.add(new GameCharacter("Game Character #" + i, 1));
        }

        List<Location> locations = new ArrayList<>(locationsCount);
        for (int i = 0; i < locationsCount; i++) {
            Location location = new Location("Location #" + i);
            locations.add(location);
            gameWorldBuilder.addLocation(location);
        }

        // Randomly set nearby locations() - bad decision as in this case generates a lot of islands without nearby locations
        // Better make as a chain A <-> B <-> C ...
        for (int i = 1; i < locationsCount; i++) {
            Location sourceLocation = locations.get(i);
            Location targetLocation = locations.get(i - 1);

            gameWorldBuilder.markLocationsAsNearbyToEachOther(sourceLocation, targetLocation);
        }

        // Randomly place game characters
        for (int i = 0; i < gameCharactersCount; i++) {
            int randomLocationToPlace = getNextRandomInt(locationsCount);
            locations.get(randomLocationToPlace).putGameCharacter(gameCharacters.get(i));
        }

        GameWorld gameWorld = gameWorldBuilder.build();

        // START EMULATING :D
        final int beforeSaveEmulateIterations = 2;
        final int afterResumeEmulateIterations = 5;
        LOGGER.info("Start emulating at " + new Date());
        for (int iteration = 0; iteration < beforeSaveEmulateIterations; iteration++) {
            LOGGER.info("Before Save Iteration #" + iteration);
            for (GameCharacter gameCharacter : gameCharacters) {
                // add random to skip sometimes explore step
                if (gameCharacter.isAlive() && getNextRandomInt(3) == 1) {
                    List<Action> availableActions = gameCharacter.explore();

                    if (!availableActions.isEmpty()) {
                        // Make random action
                        Action action = availableActions.get(getNextRandomInt(availableActions.size()));
                        LOGGER.info(String.format("Before '%s' action: %s", action.getClass().getSimpleName(), getOverallGameCharacterInfo(gameCharacter)));
                        action.execute();
//                        LOGGER.info(getOverallGameCharacterInfo(gameCharacter));
                        if (action.getClass().equals(FightAction.class)) {
                            LOGGER.info(getOverallGameCharacterInfo(gameCharacter));
                            LOGGER.info(getOverallGameCharacterInfo(((FightAction) action).getSecondGameCharacter()));
                        }

                    }
                }
            }
        }

        LOGGER.info("Save Game World");
        // SAVE AND RESUME
        gameCharacters = new ArrayList<>(gameCharacters.size());
        locations = new ArrayList<>(locations.size());

        persistenceService.saveGameWorld(gameWorld);
        gameWorld = persistenceService.resumeSavedGame(persistenceService.findSavedFileDates().get(0));

        for (Location location : gameWorld.getMapLocations()) {
            gameCharacters.addAll(location.getGameCharacters());
        }
        locations = gameWorld.getMapLocations();

        LOGGER.info("Continue processing...");
        // RESUME EMULATION
        for (int iteration = 0; iteration < afterResumeEmulateIterations; iteration++) {
            LOGGER.info("After Resume Iteration #" + iteration);
            for (GameCharacter gameCharacter : gameCharacters) {
                // add random to skip sometimes explore step
                if (gameCharacter.isAlive() && getNextRandomInt(3) == 1) {
                    List<Action> availableActions = gameCharacter.explore();

                    if (!availableActions.isEmpty()) {
                        // Make random action
                        Action action = availableActions.get(getNextRandomInt(availableActions.size()));
                        LOGGER.info(String.format("Before '%s' action: %s", action.getClass().getSimpleName(), getOverallGameCharacterInfo(gameCharacter)));
                        action.execute();
//                        LOGGER.info(getOverallGameCharacterInfo(gameCharacter));
                        if (action.getClass().equals(FightAction.class)) {
                            LOGGER.info(getOverallGameCharacterInfo(gameCharacter));
                            LOGGER.info(getOverallGameCharacterInfo(((FightAction) action).getSecondGameCharacter()));
                        }

                    }
                }
            }
        }


        LOGGER.info("Finish emulating at " + new Date());
        String overallCharactersReport = "";
        for (GameCharacter gameCharacter : gameCharacters) {
            overallCharactersReport += getOverallGameCharacterInfo(gameCharacter) + "\n";
        }
        LOGGER.info(String.format("\nOverall characters report:\n%s", overallCharactersReport));
    }
}
