# README #

This is a simple RPG with Java Virtual World in mind.
To start it just:

1. mvn clean test assembly:single
1. java -jar target/role-game-1.0-SNAPSHOT-jar-with-dependencies.jar

or in one line: *mvn clean test assembly:single && java -jar target/role-game-1.0-SNAPSHOT-jar-with-dependencies.jar*

*Notes:* 

1. For quick understand the domain model, there is uml diagram in Gliffy - uml/Domain.gliffy.
1. You can play with simulation in *TestEmulateWorld* test. Just update count of game characters, map locations count and number of iterations. After run you can find a lot of actions in your world and when iterations will finish you can find statistic for each game character, sometimes it's interesting to know is there is only one who  still alive, or 2... :)